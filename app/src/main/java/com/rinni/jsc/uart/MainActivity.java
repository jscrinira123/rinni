package com.rinni.jsc.uart;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rinni.jsc.GPIO;
import com.rinni.jsc.R;
import com.rinni.jsc.bluetooth.BluetoothConnectionService;
import com.rinni.jsc.connections.ClientClass;
import com.rinni.jsc.connections.FileServerAsyncTask;
import com.rinni.jsc.connections.ServerClass;
import com.rinni.jsc.i2c.I2C;
import com.rinni.jsc.util.ColorUser;
import com.rinni.jsc.util.Constants;
import com.rinni.jsc.util.HRUser;
import com.rinni.jsc.util.I2cInstance;
import com.rinni.jsc.util.UartInstance;
import com.rinni.jsc.util.Util;
import com.rinni.jsc.wifi.OnConnect;
import com.rinni.jsc.wifi.OnConnectedListener;
import com.rinni.jsc.wifi.WifiActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;

public class MainActivity extends AppCompatActivity implements Runnable, OnConnectedListener {

    private TextView mLog,text;
    private EditText mEditText, hexInput;
    private Button btSend, ClearButton;
    private RadioGroup group, group_form;
    private RadioButton rb, rb1;
    private LinearLayout lin_input;

    private Uart mUart;
    private static final int MESSAGE_LOG = 1;
    private Thread mThread;
    private boolean isflag = false;
    private boolean write_flag=false;
    private boolean print_flag = false;
    private static boolean rfid_stop=false;
    private String mRb_str, mRb1_str;
    private int fd;
    int length_extract = 0;
    Timer timer = new Timer();
    private boolean fp_flag = false;
    private String str="";
    String Unique_ID;

    GPIO mgpio;
    String date_time;

    String decimalVal = "";
    TextToSpeech textToSpeech;

    // LCD
    private I2C mI2C;
    private String strLCD="";
    private static boolean lcd_flag;
    private boolean io_exp_off = false;
    private int fdLCD;
    String line1 = "";
    String line2 = "";
    int row1 = 0;
    int row2 = 1;
    private Thread lcdThread;

    String ID = "";
    String NAME = "";
    String CODE = "";

    boolean isScanned = false;
    boolean isSwipe = false;
    boolean isPresence = false;

    SharedPreferences pref;

    //// MQTT

//    PubSubDemo pubSubDemo;
//    AWSIotMqttManager mqttManager;

    String siteCode;

    // WIFI
    String MAC_ADDRESS;

    //WiFi

    WifiManager mWifiManager;


    WifiP2pManager mP2PManager;
    WifiP2pManager.Channel mP2PChannel;
    BroadcastReceiver mP2PReceiver;
    IntentFilter mP2PIntentFilter;

    //Instances

    //Instances
    ClientClass clientClass;
    ServerClass serverClass;

    String TAG = MainActivity.class.getSimpleName();

    List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    String deivceNameArray[];
    WifiP2pDevice deviceArray[];

    static final int MESSAGE_READ = 1;
    static String msg;

    ArrayList<HRUser> arrHrUser;

    ArrayList<ColorUser> arrColorUser;

    boolean isFirst = false;
    boolean isPressed = false;

    //String connectionStatus;

    //WiFi List

    private final List<Integer> blockedKeys = new ArrayList<>(Arrays.asList(KeyEvent.KEYCODE_POWER));

    private final AtomicBoolean running = new AtomicBoolean(false);

    boolean isPowerPressed = false;

    // Connect Listener

    static OnConnect onConnect;

    HashMap<String, String> map = new HashMap<String, String>();

    static Util util;

    // Bluetooth

    BluetoothAdapter mBluetoothAdapter;
    BluetoothConnectionService mBluetoothConnection;

    public static final String FILTER_ACTION = "com.rinira.justschedule.RESPONSE";

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();

    ArrayList<BluetoothDevice> arrayBluetoothDevices;

    public static final int RequestPermissionCode = 1;

    EditText edtMsg;


    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch(state){
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        Toast.makeText(MainActivity.this, "Bluetooth is ON", Toast.LENGTH_LONG).show();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    /**
     * Broadcast Receiver for changes made to bluetooth states such as:
     * 1) Discoverability mode on/off or expire.
     */
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {

                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //Device is in Discoverable Mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Enabled.");
                        break;
                    //Device not in discoverable mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Able to receive connections.");
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Not able to receive connections.");
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "mBroadcastReceiver2: Connecting....");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "mBroadcastReceiver2: Connected.");
                        break;
                }

            }
        }
    };

    /**
     * Broadcast Receiver for listing devices that are not yet paired
     * -Executed by btnDiscover() method.
     */
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive: ACTION FOUND.");

            if (action.equals(BluetoothDevice.ACTION_FOUND)){
                BluetoothDevice device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);

                Log.d(TAG, "onReceive: " + device.getName());

                if (mBTDevices.contains(device)) {

                } else {
                    mBTDevices.add(device);
                }
//                Log.d(TAG, "onReceive: " + device.getName() + ": " + device.getAddress());
//                mDeviceListAdapter = new BluetoothDeviceListAdapter(context, R.layout.device_ble_adapter_view, mBTDevices);
//                lvNewDevices.setAdapter(mDeviceListAdapter);
            }
        }
    };

    /**
     * Broadcast Receiver that detects bond state changes (Pairing status changes)
     */
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED){
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    Toast.makeText(MainActivity.this, "BONDED", Toast.LENGTH_LONG).show();

                    edtMsg.requestFocus();
                    //inside BroadcastReceiver4
                //    mBluetoothConnection = new BluetoothConnectionService(MainActivity.this);
                    // mBluetoothConnection.setOnBluetoothResponseListener(MainActivity.this);

                    try {
                        Thread.sleep(500);
                        //   deviceModeChange("001", 1000, Constants.SPEAK_WIFI_CONNECTED_SUCCESS);

                        textToSpeech.speak(Constants.SPEAK_BLUETOOTH_CONNECTED_SUCCESS, TextToSpeech.QUEUE_FLUSH, map);
                        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                            @Override
                            public void onStart(String utteranceId) {
                            }

                            @Override
                            public void onDone(String utteranceId) {

                                try {
                                    Thread.sleep(500);

                                    textToSpeech.speak(Constants.SPEAK_RFID_MODE, TextToSpeech.QUEUE_FLUSH, map);
                                    textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                        @Override
                                        public void onStart(String utteranceId) {

                                        }

                                        @Override
                                        public void onDone(String utteranceId) {

                                            try {
                                                Thread.sleep(500);
                                                deviceModeChangeAfterConnection("001", 500, Constants.DEFWELCOMETEXT);
                                                isFirst = true;
                                                isPressed = false;
                                                running.set(true);

                                                mThread = new Thread(MainActivity.this);
                                                mThread.start();
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onError(String utteranceId) {

                                        }
                                    });


                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(String utteranceId) {

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Toast.makeText(MainActivity.this, "BONDING", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {

                    Toast.makeText(MainActivity.this, "FAILED TO BOND", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };

    private BroadcastReceiver myBroadcastReceiver5 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive: ACTION FOUND.");

            if (action.equals(FILTER_ACTION)){

                String data = intent.getStringExtra("data");

                if (data.contains("bluetooth")) {

                    Log.d("Read Msg", "Bluetooth "+ data);
                    connectBluetooth(data);
                } else {
                    Log.d("Read Msg", "Wifi "+ data);
                    connectWiFi(data);
                }

              //  LCDBluetoothText(value);

            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();

//        if (mBroadcastReceiver2 != null) {
//            unregisterReceiver(mBroadcastReceiver2);
//        }
//        if (mBroadcastReceiver4 != null) {
//            unregisterReceiver(mBroadcastReceiver4);
//        }
//
        if (myBroadcastReceiver5 != null) {
            unregisterReceiver(myBroadcastReceiver5);
        }
//        if(mP2PReceiver != null) {
//            unregisterReceiver(mP2PReceiver);
//        }
//        mBluetoothAdapter.cancelDiscovery();
//        unregisterReceiver(mBroadcastReceiver1);
//        unregisterReceiver(mBroadcastReceiver2);
//        unregisterReceiver(mBroadcastReceiver3);
//        unregisterReceiver(mBroadcastReceiver4);
        //mBluetoothAdapter.cancelDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(mBroadcastReceiver1);
//        unregisterReceiver(mBroadcastReceiver2);
//        unregisterReceiver(mBroadcastReceiver3);
//        unregisterReceiver(mBroadcastReceiver4);
        //mBluetoothAdapter.cancelDiscovery();
    }


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver4, filter);
        registerReceiver(myBroadcastReceiver5, new IntentFilter(FILTER_ACTION));

    }

    public void enableDiscoverableBT(){

        if(mBluetoothAdapter == null){
            Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
        }
        if(!mBluetoothAdapter.isEnabled()){
            Log.d(TAG, "enableDisableBT: enabling BT.");
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }

    }
//    @Override
//    public void onResponse(String response) {
//
//        LCDBluetoothText(response);
//
//
//    }

    private void LCDBluetoothText(String strLCD) {

        if(lcd_flag == true) {
            mI2C.IO_set(3, false);                                              /*Make LCD_RST high*/
            mI2C.IO_out(3, 1);

            mI2C.IO_set(2, false);                                              /*Enable GPS_EN pin of I/O expander*/
            mI2C.IO_out(2, 1);

            mI2C.PutChar(0, row1, "                ");
            mI2C.PutChar(0, row2, "                ");

            /*Initialise LCD*/

            if (strLCD.length() == 0) {

                mI2C.PutChar(0, row1, "                ");
                mI2C.PutChar(0, row2, "                ");
            }

            else if (strLCD.length() <= 16) {
                mI2C.PutChar(0, row1, strLCD);
                mI2C.PutChar(0, row2, "                ");
            } else {

                String firstRow = strLCD.substring(0, 15);
                String secondRow = strLCD.substring(16, strLCD.length()-1);
                mI2C.PutChar(0, row1, firstRow);
                mI2C.PutChar(0, row1, secondRow);
            }
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                LCDText(Constants.DEFWELCOMETEXT, util.getStatus());
                setColor("001");
            }
        }, 10000);


    }

    private void setColor(String color) {

        int DEFAULT_PIN = 911;
        int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
        int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
        int blue_pin = 69 + DEFAULT_PIN;
        /*Define the Blue Pin number*/
        /*Check for Valid conditions for the RGB LED*/

        char red_value = color.charAt(0);
        char green_value = color.charAt(1);
        char blue_value = color.charAt(2);

        mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
        mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
        mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_i2c);

        enableRuntimePermission();

        edtMsg = (EditText) findViewById(R.id.edtMsg);
        edtMsg.requestFocus();

        util = new Util();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        enableDiscoverableBT();
        mBTDevices = new ArrayList<>();

        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "Main");
        onConnect = new OnConnect(this);

        isFirst = true;
        if (Util.checkInternet(this)) {
            util.setStatus("R");
        } else {
            util.setStatus("N");
        }

        pref = this.getSharedPreferences("JSC", 0);

        initArrays();
        initRFID();
        initNetworks();
    }

    public void enableRuntimePermission() {

        ActivityCompat.requestPermissions(this, new String[]
                {
                        ACCESS_COARSE_LOCATION,
                }, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {
                    Log.d(TAG, "WiFi Permission Granted");
                }
                break;
        }
    }

    private void checkBTPermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = ActivityCompat.checkSelfPermission(this,"Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += ActivityCompat.checkSelfPermission(this,"Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }else{
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    private void initRFID() {

        mgpio= new GPIO();
        mUart = UartInstance.getInstance();

        MAC_ADDRESS = Util.getMACAddress(this);
        Log.d("MAC", MAC_ADDRESS);
        textToSpeech = Util.getTTSInstance(this);
        // LCD
        strLCD = "";                                                 /*If any extra other than label "string" or related to io expander is found, print the default text on LCD*/
        lcd_flag = true;
        io_exp_off = false;

        mI2C = I2cInstance.getInstance();
        LCDText(Constants.DEFWELCOMETEXT, util.getStatus());

        /*Start the thread for receiving*/
        mThread = new Thread(MainActivity.this);
        mThread.start();
        /*Start the thread for receiving*/
    }

    private void initNetworks() {
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }

        mP2PManager = (WifiP2pManager) getApplicationContext().getSystemService(Context.WIFI_P2P_SERVICE);
        mP2PChannel = mP2PManager.initialize(this, getMainLooper(), null);

        deletePersistentGroups();

        mP2PReceiver = new MainDirectBroadcastReceiver(mP2PManager, mP2PChannel, MainActivity.this);
        mP2PIntentFilter = new IntentFilter();

        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    private void deletePersistentGroups() {
        try {
            Method[] methods = WifiP2pManager.class.getMethods();
            for (int i = 0; i < methods.length; i++) {
                if (methods[i].getName().equals("deletePersistentGroup")) {
                    // Delete any persistent group
                    for (int netid = 0; netid < 32; netid++) {
                        methods[i].invoke(mP2PManager, mP2PChannel, netid, null);
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!hasFocus) {

//            if (!isConnectedOrConnecting(peers)) {

            Log.d(TAG, "isPowerPressed not Invited" + isPowerPressed);

            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);

            // initialize P2P on long press.
            Log.d("WiFi", "Long Pressed 2");

            if (isFirst == false && isPressed == true) {

                isFirst = true;
                isPressed = false;
                running.set(true);
                if (Util.checkInternet(this)) {
                    util.setStatus("R");
                } else {
                    util.setStatus("N");
                }
                try {
                    Thread.sleep(500);
                    deviceModeChange("001", 1000, Constants.MSG_RFID_MODE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(500);
                    LCDText(Constants.DEFWELCOMETEXT, util.getStatus());
                    mThread = new Thread(MainActivity.this);
                    mThread.start();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            else if (isFirst) {
                util.setStatus("W");
                isFirst = false;
                isPressed = true;
//                mThread.interrupt();
                running.set(false);
                deviceModeChange("110", 1000, Constants.MSG_WIFI_MODE);
                registerReceiver(mP2PReceiver, mP2PIntentFilter);
                discoverP2PNetworks();
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onConnected() {

        Log.d("Connected", "success");
        try {
            Thread.sleep(500);
            //   deviceModeChange("001", 1000, Constants.SPEAK_WIFI_CONNECTED_SUCCESS);

            textToSpeech.speak(Constants.SPEAK_WIFI_CONNECTED_SUCCESS, TextToSpeech.QUEUE_FLUSH, map);
            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                }

                @Override
                public void onDone(String utteranceId) {

                    try {
                        Thread.sleep(500);

                        textToSpeech.speak(Constants.SPEAK_RFID_MODE, TextToSpeech.QUEUE_FLUSH, map);
                        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                            @Override
                            public void onStart(String utteranceId) {

                            }

                            @Override
                            public void onDone(String utteranceId) {

                                try {
                                    Thread.sleep(500);
                                    deviceModeChangeAfterConnection("001", 500, Constants.DEFWELCOMETEXT);
                                    isFirst = true;
                                    isPressed = false;
                                    running.set(true);

                                    mThread = new Thread(MainActivity.this);
                                    mThread.start();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(String utteranceId) {

                            }
                        });


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String utteranceId) {

                }
            });

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void connectBluetooth(String bluetoothAddress) {

        Log.d("Read Msg", "Bluetooth 1");
        String address = bluetoothAddress.split(",")[1].trim();
        Log.d("Read Msg", "Bluetooth 2");

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        try {
            createBond(device);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void createBond(BluetoothDevice device) {
//
//        try {
//
//            String BLE_PIN = "1234";
//            Log.d("pairDevice()", "Start Pairing...");
//            Method m = device.getClass().getMethod("createBond", (Class[]) null);
//
//            m.invoke(device, (Object[]) null);
//            device.setPin(BLE_PIN.getBytes());
//            Log.d("pairDevice()", "Pairing finished.");
//        } catch (Exception e) {
//            Log.e("pairDevice()", e.getMessage());
//        }
//    }

//    private void unpairDevice(BluetoothDevice device) {
//        try {
//            Method m = device.getClass()
//                    .getMethod("removeBond", (Class[]) null);
//            m.invoke(device, (Object[]) null);
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
//        }
//
//        createBond(device);
//    }

    public boolean createBond(BluetoothDevice btDevice)
            throws Exception {
        Log.d("Read Msg", "Bluetooth 3");
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Log.d("Read Msg", "Bluetooth 4");
        Method createBondMethod = class1.getMethod("createBond");
        Log.d("Read Msg", "Bluetooth 5");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        Log.d("Read Msg", "Bluetooth 6");
        return returnValue.booleanValue();
    }

//    private void createBond(BluetoothDevice device) {
//
//        Log.d("Read Msg", "Bluetooth 4");
//        try {
//            Log.d("pairDevice()", "Start Pairing...");
//            Method m = device.getClass().getMethod("createBond", (Class[]) null);
//
//            Log.d("Read Msg", "Bluetooth 5");
//            m.invoke(device, (Object[]) null);
//            Log.d("Read Msg", "Bluetooth 6");
//
//            //device.setPin(BLE_PIN.getBytes());
//            Log.d("pairDevice()", "Pairing finished.");
//        } catch (Exception e) {
//            Log.e("pairDevice() ", e.getMessage());
//        }
//    }

    private void connectWiFi(final String wifiName) {

        String SSID = wifiName.split(",")[0].trim();

        String passKey = wifiName.split(",")[1].trim();

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", SSID);
        wifiConfig.preSharedKey = String.format("\"%s\"", passKey);
        int netId = mWifiManager.addNetwork(wifiConfig);
        mWifiManager.disconnect();
        mWifiManager.enableNetwork(netId, true);
        boolean isConnected = mWifiManager.reconnect();

        Log.d("Read Msg isConnected", "Msg "+ isConnected);

        if (isConnected) {
            msg = "WiFi Connected";
            new FileServerAsyncTask(MainActivity.this).execute(msg);
        }
    }

//    public static class FileServerAsyncTask extends AsyncTask<Void, Void, String> {
//
//        private Context context;
//        private TextView statusText;
//
//        /**
//         * @param context
//         */
//        public FileServerAsyncTask(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        protected String doInBackground(Void... params) {
//
//
//            util.getSendReceive().write(msg.getBytes());
//            return "";
//
//        }
//
//        /*
//         * (non-Javadoc)
//         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
//         */
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//
//                onConnect.onConnect();
//            }
//        }
//
//        /*
//         * (non-Javadoc)
//         * @see android.os.AsyncTask#onPreExecute()
//         */
//        @Override
//        protected void onPreExecute() {
//
//        }
//
//    }

    private void disConnect() {

        mP2PManager.removeGroup(mP2PChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onFailure(int reasonCode) {
                Log.d(TAG, "Disconnect failed. Reason :" + reasonCode);

            }
            @Override
            public void onSuccess() {
                Log.d(TAG, "Disconnect success. Reason :");
            }
        });
    }

    public WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {

            Log.d("PeerList", "List : " + peerList);

            if (!peerList.getDeviceList().equals(peers)) {
                peers.clear();
                peers.addAll(peerList.getDeviceList());
                deivceNameArray = new String[peerList.getDeviceList().size()];
                deviceArray = new WifiP2pDevice[peerList.getDeviceList().size()];
                int index = 0;

                for (WifiP2pDevice device : peerList.getDeviceList()) {

                    deivceNameArray[index] = device.deviceName;
                    deviceArray[index] = device;
                    index++;
                }
                // set Adapter
            }

            if (peers.size() == 0) {
                Log.d(TAG, "No Device Found");
            }

        }
    };

    public WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {

            final InetAddress groupOwner = wifiP2pInfo.groupOwnerAddress;

            if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {

                Log.d(TAG, "Server");

                textToSpeech.speak(Constants.SPEAK_WIFI_DIRECT, TextToSpeech.QUEUE_FLUSH, map);
                textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {
                    }
                    @Override
                    public void onDone(String utteranceId) {

                        Log.d(TAG, "ServerClass 1" );
                        serverClass = new ServerClass(MainActivity.this);
                        Log.d(TAG, "ServerClass 3" );
                        serverClass.start();

//                        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//                        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 100);
//                        startActivity(discoverableIntent);
//
//                        IntentFilter intentFilter = new IntentFilter(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
//                        registerReceiver(mBroadcastReceiver2,intentFilter);

                        if(mBluetoothAdapter.isDiscovering()) {
                            mBluetoothAdapter.cancelDiscovery();
                            Log.d(TAG, "btnDiscover: Canceling discovery.");

                            //check BT permissions in manifest
                            enableRuntimePermission();

                            mBluetoothAdapter.startDiscovery();
                            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                        }
                        else if(!mBluetoothAdapter.isDiscovering()) {

                            //check BT permissions in manifest
                            enableRuntimePermission();

                            mBluetoothAdapter.startDiscovery();
                            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                        }
                    }

                    @Override
                    public void onError(String utteranceId) {

                    }
                });

            } else  if(wifiP2pInfo.groupFormed) {

                Log.d(TAG, "Client");

                textToSpeech.speak(Constants.SPEAK_WIFI_DIRECT, TextToSpeech.QUEUE_FLUSH, map);
                textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {
                    }
                    @Override
                    public void onDone(String utteranceId) {

                        Log.d(TAG, "ClientClass 1");
                        clientClass = new ClientClass(MainActivity.this, groupOwner);
                        Log.d(TAG, "ClientClass 7" );
                        clientClass.start();
                        Log.d(TAG, "ClientClass 12" );

//                        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//                        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 100);
//                        startActivity(discoverableIntent);
//
//                        IntentFilter intentFilter = new IntentFilter(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
//                        registerReceiver(mBroadcastReceiver2,intentFilter);

                        if(mBluetoothAdapter.isDiscovering()) {
                            mBluetoothAdapter.cancelDiscovery();
                            Log.d(TAG, "btnDiscover: Canceling discovery.");

                            //check BT permissions in manifest
                            enableRuntimePermission();

                            mBluetoothAdapter.startDiscovery();
                            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                        }
                        else if(!mBluetoothAdapter.isDiscovering()) {

                            //check BT permissions in manifest
                            enableRuntimePermission();

                            mBluetoothAdapter.startDiscovery();
                            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                        }
                    }

                    @Override
                    public void onError(String utteranceId) {

                    }
                });
            }
        }
    };

    private void discoverP2PNetworks() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WifiP2pManager.ActionListener actionListener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int i) {

                Log.d(TAG, "Discovery Failed");
            }
        };

        //  mP2PManager.removeGroup(mP2PChannel, actionListener);
        mP2PManager.discoverPeers(mP2PChannel, actionListener);

    }

    @Override
    public void run() {

        Log.d(TAG, "Thread started");
        running.set(true);

        while (running.get()) {

            Bundle extras = getIntent().getExtras();
            Intent intent = getIntent();

            if (intent.hasExtra("send"))          /*Check if the received parameter from Command line contains "send" as label, if contains store the string to the str string*/ {
                print_flag = true;
                str = intent.getStringExtra("send");        /*Extract the string with the label send*/
                rfid_stop = true;

            } else {
//            print_flag = false;                     /*To exit the app, if the app was opened without any arguements*/
                print_flag = true;
                //  str = "AA028000020052CDC4";
                str = "AA028000020052CDC4";

                /*If any extra other than label "send" is found, send the default command on UART*/
            }
            length_extract = str.length();
            if (length_extract > 32)               //Command for finger print if polling is given
            {
                fp_flag = true;
            } else {
                fp_flag = false;
            }
            /****************************************************/
            byte[] hex_buffer;
            isflag = false;
            isflag = true;

            if (print_flag) {

                String mEditStr = str;
                hex_buffer = hexStringToByteArray(mEditStr);        //Convert the string to the hex data
                if (mEditStr.isEmpty()) {
                    Toast.makeText(MainActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                    return;
                }

                int ret = mUart.write(hex_buffer, hex_buffer.length);       //Send the data over UART

                if (ret > 0)                                         /*Check if write was successful*/ {
                    write_flag = true;
                    Log.d("Error", "Write Success");
                } else if (ret < 0)                                /*Write Fail, Print Error Message*/ {
                    write_flag = false;
                    Log.d("Error", "Not Written the value");
                    Toast.makeText(MainActivity.this, " Write Failure..!", Toast.LENGTH_SHORT).show();
                }
            }


            if ((isflag == true) && (write_flag == true) && (print_flag == true)) {
                long start_time, wait_time, end_time;
                if (fp_flag == true)                                       //If command send from command line for FP, to avoid reading for fp scanning time is changed to100
                {
                    start_time = System.currentTimeMillis();
                    wait_time = 100;
                    end_time = start_time + wait_time;
                } else                                                      //If command send from command line for RFID
                {
                    start_time = System.currentTimeMillis();
                    wait_time = 500;
                    end_time = start_time + wait_time;
                }
                boolean has_data = false;
                boolean no_data = false;
                byte[] receive_buff = new byte[2048];
                byte[] receive_buff_finger = new byte[2048];
                String final_hex_fngr = "";
                String final_hex_buff = "";
                String INSTRUCTION = "RESPONSE";
                String packet_extract = "";
                String compare = "aa55";
                String compare_identify = "aa550201";
                int ret_count = 0;
                int return_length = 0;
                boolean one_time = false;
                while (System.currentTimeMillis() < end_time) {

                    int val = mUart.read(receive_buff, receive_buff.length);                //Read Hex data from UART
                    final_hex_buff = byteArrayToHex(receive_buff);

                    Log.d(INSTRUCTION, "Response final_hex_buff:" + final_hex_buff);
                    //Convert Hex data to String
//                int len = final_hex_buff.length();

                    if ((val == -1)) {
                        no_data = true;

                        Log.d("RETURN", "T+RETURN " + val);
                    } else {
                        Log.d("RETURN", "RETURN " + val);
                        has_data = true;

                        return_length = val;
                        /***************************/

                        Log.d(INSTRUCTION, "VALUE : " + val);
                    }
                }
                Log.d("RETURN", "PACKET" + final_hex_buff);

//            while (System.currentTimeMillis() < end_time)
                if (has_data == true && fp_flag == false)                    /*Check for RFID extraction*/ {
                    no_data = false;
                    String check_buff = "";
                    String Calculate_CRC_Buffer = "";
                    String Calculate_CRC_Buffer_Resp1 = "";
                    String Calculate_CRC_Buffer_Resp2 = "";
                    String Calculate_CRC_Buffer_UID_Resp = "";
                    String CRC_Buffer = "";
                    String CRC_Buffer_Resp1 = "";
                    String CRC_Buffer_Resp2 = "";
                    String CRC_Buffer_UID_Resp = "";
                    String Resp1 = "";
                    String UID_Resp = "";
                    String Resp2 = "";
                    String CRC_RESP1 = "";
                    String CRC_RESP2 = "";
                    String CRC_UID = "";
                    String CRC = "";

                    for (int i = 0; i < 2 * return_length; i++)
                        check_buff += final_hex_buff.charAt(i);                             //Final string after removing excess of data(i.e. zero as length of buffer length)
                    //////////////////////////////////////////////

                    Log.d(INSTRUCTION, "Response check_buff:" + check_buff);


                    int checkbuff_len = check_buff.length();
//                //Extraction of String for which the CRC must be calculated
//                for (int j = 18; j < (checkbuff_len - 20); j++) {//2,4
//                    Calculate_CRC_Buffer += check_buff.charAt(j);
//                    /*Copy the receieved CRC value to a buffer*/
//                    if (j == (checkbuff_len - 21))//5              //CRC starting index?
//                    {
//                        for (int k = (checkbuff_len - 20); k < checkbuff_len - 16; k++) {//4
//                            CRC_Buffer += check_buff.charAt(k);         //Copy the CRC value
//                        }
//                    }
//                    /*Copy the receieved CRC value to a buffer*/
//                }

                    if (checkbuff_len > 32)                                                     //when card present, the data which is received from UART will be more than 32 char
                    {
                        for (int j = 0; j < 16; j++) {
                            Resp1 += final_hex_buff.charAt(j);                    //1st response

                        }
                        for (int l = 16; l < 48; l++) {                          //for card uid
                            UID_Resp += final_hex_buff.charAt(l);

                        }
                        for (int m = 48; m < 64; m++) {                            //3rd response
                            Resp2 += final_hex_buff.charAt(m);

                        }
                        for (int j = 2; j < (Resp1.length() - 4); j++) {
                            Calculate_CRC_Buffer_Resp1 += Resp1.charAt(j);          //Buffer to calculate the CRC
                            /*Copy the receieved CRC value to a buffer*/
                            if (j == (Resp1.length() - 5))          //CRC starting index?
                            {
                                for (int k = (Resp1.length() - 4); k < Resp1.length(); k++) {
                                    CRC_Buffer_Resp1 += Resp1.charAt(k);         //Copy the CRC value
                                }
                            }
                            /*Copy the receieved CRC value to a buffer*/
                        }


                        for (int j = 2; j < (Resp2.length() - 4); j++) {
                            Calculate_CRC_Buffer_Resp2 += Resp2.charAt(j);          //Buffer to calculate the CRC
                            /*Copy the receieved CRC value to a buffer*/
                            if (j == (Resp2.length() - 5))          //CRC starting index?
                            {
                                for (int k = (Resp2.length() - 4); k < Resp2.length(); k++) {
                                    CRC_Buffer_Resp2 += Resp2.charAt(k);         //Copy the CRC value
                                }
                            }
                        }
                        /*Copy the receieved CRC value to a buffer*/

                        for (int j = 2; j < (UID_Resp.length() - 4); j++) {
                            Calculate_CRC_Buffer_UID_Resp += UID_Resp.charAt(j);         //Buffer to calculate the CRC
                            /*Copy the receieved CRC value to a buffer*/
                            if (j == (UID_Resp.length() - 5))     //CRC starting index?
                            {
                                for (int k = (UID_Resp.length() - 4); k < UID_Resp.length(); k++) {
                                    CRC_Buffer_UID_Resp += UID_Resp.charAt(k);         //Copy the CRC value
                                }
                            }
                            /*Copy the receieved CRC value to a buffer*/
                        }
                        int CRC_VAL_RESP1 = CRC16_BUYPASS(Calculate_CRC_Buffer_Resp1);        //Compute CRC for extracted data excluding header
                        CRC_RESP1 = Integer.toHexString(CRC_VAL_RESP1);                    //CRC value to the string

                        int CRC_VAL_RESP2 = CRC16_BUYPASS(Calculate_CRC_Buffer_Resp2);        //Compute CRC
                        CRC_RESP2 = Integer.toHexString(CRC_VAL_RESP2);                    //CRC value to the string

                        int CRC_VAL_UID = CRC16_BUYPASS(Calculate_CRC_Buffer_UID_Resp);   //Compute CRC
                        CRC_UID = Integer.toHexString(CRC_VAL_UID);                       //CRC value to the string
                    } else {
                        //Extraction of String for which the CRC must be calculated
                        for (int j = 2; j < (checkbuff_len - 4); j++) {
                            Calculate_CRC_Buffer += check_buff.charAt(j);
                            /*Copy the receieved CRC value to a buffer*/
                            if (j == (checkbuff_len - 5))           //CRC starting index?
                            {
                                for (int k = (checkbuff_len - 4); k < checkbuff_len; k++) {
                                    CRC_Buffer += check_buff.charAt(k);         //Copy the CRC value
                                }
                            }
                            /*Copy the receieved CRC value to a buffer*/
                        }
                        int CRC_VAL = CRC16_BUYPASS(Calculate_CRC_Buffer);                  //Compute CRC
                        CRC = Integer.toHexString(CRC_VAL);

                    }
                    int length = check_buff.length();                                       //Check the size of the received string
                    if (length > 32)                                                         //condition to check if length of the data received is greater than 32(to check the CRC of 3 response packet is matching with their corresponding CRC val)
                    {
                        if ((CRC_Buffer_Resp1.equals(CRC_RESP1)) && (CRC_Buffer_Resp2.equals(CRC_RESP2)) && (CRC_Buffer_UID_Resp.equals(CRC_UID))) {

                            Log.d("Handler", "CRC  Matched 1");

                            String UID_buff = "";
                            for (int i = 14; i < 22; i++) {
                                UID_buff += UID_Resp.charAt(i);
                            }
                            Log.d("Handler", "CRC  Matched 2");

                            Unique_ID = UID_buff.toUpperCase();

                            decimalVal = Util.convertToDecimal(Unique_ID);

                            if (decimalVal.length() < 10) {

                                decimalVal = "0" + decimalVal;
                            }

                            Log.d("Decimal Value", "value is " + Unique_ID);
                            Log.d("Decimal Value", "value is " + decimalVal);

                            ID = "";
                            NAME = "";
                            CODE = "";
                            isScanned = false;
                            isSwipe = false;
                            isPresence = false;

                            for (HRUser hrUser : arrHrUser) {

                                if (UID_buff.toUpperCase().equalsIgnoreCase(hrUser.getId())) {

                                    ID = hrUser.getId();
                                    NAME = hrUser.getName();
                                    Log.d("Decimal", "Value is 1 : " + ID);
                                    Log.d("Decimal", "Value is 1 : " + NAME);
                                }
                            }

                            for (ColorUser colorUser : arrColorUser) {

                                if (UID_buff.toUpperCase().equalsIgnoreCase(colorUser.getId())) {

                                    ID = colorUser.getId();
                                    NAME = colorUser.getName();
                                    CODE = colorUser.getCode();

                                    Log.d("Decimal", "Value is 1 : " + ID);
                                    Log.d("Decimal", "Value is 1 : " + NAME);
                                    Log.d("Decimal", "Value is 1 : " + CODE);
                                }
                            }


                            if (ID != "" && NAME != "" && CODE == "") {

                                isScanned = true;
                                isSwipe = false;
                                isPresence = true;

                                if (!NAME.equalsIgnoreCase("")) {

                                    changeColor("010", 0000, Constants.MSG_VALID_USER);
                                    try {
                                        Thread.sleep(2000);
                                        changeColor("001", 1000, Constants.DEFWELCOMETEXT);

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                    changeColor("100", 0000, Constants.MSG_INVALID);
                                    try {
                                        Thread.sleep(2000);
                                        changeColor("001", 1000, Constants.DEFWELCOMETEXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else if (ID != "" && NAME != "" && CODE != "") {

                                isScanned = true;
                                isSwipe = true;
                                isPresence = false;

                                if (!NAME.equalsIgnoreCase("")) {

                                    changeColor(CODE, 0000, Constants.MSG_VALID_COLOR);
                                    try {
                                        Thread.sleep(2000);
                                        changeColor("001", 1000, Constants.DEFWELCOMETEXT);

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                    changeColor("100", 0000, Constants.MSG_INVALID);
                                    try {
                                        Thread.sleep(2000);
                                        changeColor("001", 1000, Constants.DEFWELCOMETEXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }

                            } else {

                                changeColor("100", 0000, Constants.MSG_INVALID);
                                try {
                                    Thread.sleep(2000);
                                    changeColor("001", 1000, Constants.DEFWELCOMETEXT);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }


//                            if (UID_buff.toUpperCase().equalsIgnoreCase("47ACE60F") ||
//                                    UID_buff.toUpperCase().equalsIgnoreCase("6699F4FA")) {
//
//                                changeColor("010", 0000, validText);
//                                try {
//                                    Thread.sleep(2000);
//                                    changeColor("001", 1000, defaultText);
//
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//
//                                changeColor("100", 0000, invalidText);
//                                try {
//                                    Thread.sleep(2000);
//                                    changeColor("001", 1000, defaultText);
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                            }


//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                //Write whatever to want to do after delay specified (1 sec)
//
//                            }
//                        }, 3000);

                            //Check if the data contains the UID
                            /*******************/
//                            String UID_buff = "";
                            for (int i = 14; i < 22; i++)
                                UID_buff += UID_Resp.charAt(i);
                            if (!UID_buff.isEmpty() && (return_length != -1)) {
                                Message m = Message.obtain(mHandler, MESSAGE_LOG);
                                //Print the UID
                                m.obj = "Response 1:" + Resp1.toUpperCase() + "\n" +
                                        "Response 2:" + Resp2.toUpperCase() + "\n" +
                                        "UID Response Data:" + UID_Resp.toUpperCase() + "\n"
                                        + "Unique ID:" + UID_buff.toUpperCase() + "\n";

                                Log.d(INSTRUCTION, "Response 1:" + Resp1.toUpperCase());
                                Log.d(INSTRUCTION, "Response 2:" + Resp2.toUpperCase());
                                Log.d(INSTRUCTION, "UID Response:" + UID_Resp.toUpperCase());
                                Log.d(INSTRUCTION, "Unique ID:" + UID_buff.toUpperCase());            //Print the response received and the UID of the card on Logcat

                                mHandler.sendMessage(m);

                            }
                            /*******************/

                            print_flag = false;
                        } else if (length > 0)                            //Data received, but CRC not matched?
                        {
                            Message m = Message.obtain(mHandler, MESSAGE_LOG);
                            //Print the UID
                            m.obj = "CRC Not Matched" + "\n";
                            Log.d(INSTRUCTION, "CRC Not Matched, Not a valid response");    //Print Error Message on Logcat
                            mHandler.sendMessage(m);
                            print_flag = false;
                        }
                    } else {
                        if (CRC.equals(CRC_Buffer))                //Check if Calculated CRC and the CRC of the received data is matching
                        {
                            if ((length <= 18) && (length > 0))                                          //Check if the data is response for the command
                            {
                                check_buff.toUpperCase();
                                if (!check_buff.isEmpty() && (return_length != -1)) {
                                    Message m = Message.obtain(mHandler, MESSAGE_LOG);
                                    //Print the response received
                                    m.obj = "Response Received:" + check_buff.toUpperCase() + "\n";
                                    Log.d(INSTRUCTION, "Response:" + check_buff.toUpperCase());        //Print the response received on Logcat
                                    mHandler.sendMessage(m);
                                    print_flag = false;
                                }
                            }
//                        else if (length > 18) {                 //Check if the data contains the UID
//
//                            String UID_buff = "";
//                            for (int i = 14; i < 22; i++)
//                                UID_buff += final_hex_buff.charAt(i);
//                            if (!UID_buff.isEmpty() && (val != -1)) {
//                                Message m = Message.obtain(mHandler, MESSAGE_LOG);
//                                //Print the UID
//                                m.obj = "Received Data:" + check_buff.toUpperCase() + "\n"
//                                        + "Unique ID:" + UID_buff.toUpperCase() + "\n";
//
//                                Log.d(INSTRUCTION, "Response:" + check_buff.toUpperCase());
//                                Log.d("Unique ID", UID_buff.toUpperCase());            //Print the response received and the UID of the card on Logcat
//                                /******************/
////                            text = (TextView)findViewById(R.id.data_received) ;
////                            text.setText("UID:"+UID_buff.toUpperCase()); //set text for text view
//                                /************/
//                                mHandler.sendMessage(m);
//                                print_flag = false;
//                            }
//                        }
                        } else if (length > 0)                            //Data received, but CRC not matched?
                        {
                            Message m = Message.obtain(mHandler, MESSAGE_LOG);
                            //Print the UID
                            m.obj = "CRC Not Matched" + "\n";
                            Log.d(INSTRUCTION, "CRC Not Matched, Not a valid response");    //Print Error Message on Logcat
                            mHandler.sendMessage(m);
                            print_flag = false;
                        }

                    }
                    rfid_stop = false;

                } else if ((has_data == true && fp_flag == true) || ((no_data == true && fp_flag == false) && (rfid_stop == false)) || (no_data == true && fp_flag == true))              /*Check for Fingeprint extraction*/ {
                    //	Log.d("RETURN", "HEX BYTE " + final_buff);

                    String Response1 = "";
                    String Response2 = "";
                    String ID = "";
                    String identify_command = "55AA02010000000000000000000000000000000000000201";
                    String final_buff = "";
                    byte[] send_command;
                    String template_no = "";
                    long start_time1; //= System.currentTimeMillis();
                    long wait_time1;// = 6000;
                    long end_time1;// = start_time1 + wait_time1;
                    //send part for finger print
                    Log.d("SEND", "Send Command for Finger Print");
                    String s = null;

                    if (has_data == false && fp_flag == false)                                 //check for
                    {
                        Log.d(INSTRUCTION, "Write..");
                        String mEditStr1 = identify_command;
                        send_command = hexStringToByteArray(mEditStr1);        //Convert the string to the hex data
                        if (mEditStr1.isEmpty()) {
                            Toast.makeText(MainActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        int ret = mUart.write(send_command, send_command.length);       //Send the data over UART

                        if (ret > 0)                                         /*Check if write was successful*/ {

                            Log.d("Error", "Write Success");
                        } else if (ret < 0)                                /*Write Fail, Print Error Message*/ {

                            Log.d("Error", "Not Written the value");

                        }
                        start_time1 = System.currentTimeMillis();
                        wait_time1 = 2500;
                        end_time1 = start_time1 + wait_time1;
                        //wait for 2 second for response
                        int val = 0;
                        while (System.currentTimeMillis() < end_time1) {
//                        Log.d("RETURN", "Inside While ");
                            val = mUart.read(receive_buff_finger, receive_buff_finger.length);                //Read Hex data from UART
//                        Log.d("RETURN", "Response-FP " + receive_buff_finger);
//                        Log.d("RETURN", "Value " + val);
                            final_hex_fngr = byteArrayToHex(receive_buff_finger);                          //Convert Hex data to String

                            for (int i = 0; i < 2 * val; i++)
                                final_buff += final_hex_fngr.charAt(i);                             //Final string after removing excess of data(i.e. zero as length of buffer length)
                            //////////////////////////////////////////////
//                            Log.d("RETURN", "Response-FP " + final_buff);

//                        if(final_hex_fngr.indexOf(compare_identify) != -1)                             //if response for finger identity 0x0201
//                         break;


                        }

                        int check_len = final_buff.length();
                        Log.d("RETURN", "L-BYTE1 " + check_len);
                        if (/*(check_len >= 48 )&&*/ check_len != 0) {
                            int c = 0, d = 0, num = 0;
                            for (c = 0; c < (check_len / 48); c++) {

                                for (d = (48 * c); d < 48 + num; d++) {

                                    Response1 += final_buff.charAt(d);
//                                Log.d("RETURN", "Response" + Response1);

                                }
                                String response = Response1.substring(12, 16);
                                Log.d(INSTRUCTION, Response1.toUpperCase());
                                Message msg = Message.obtain(mHandler, MESSAGE_LOG);
                                if (Response1.indexOf(compare_identify) != -1 && Response1.indexOf("f4ff") == -1 && response.indexOf("0000") != -1) {
                                    for (int g = 16; g < 20; g++) {
                                        template_no += Response1.charAt(g);
                                    }
                                    Log.d(INSTRUCTION, "Template Number:" + template_no);
                                    msg.obj = "Response:" + (c + 1) + "\n" + Response1.toUpperCase() + "\n" + "Template no:" + template_no + "\n";
                                } else
                                    msg.obj = "Response:" + (c + 1) + "\n" + Response1.toUpperCase() + "\n";
                                mHandler.sendMessage(msg);
                                Response1 = "";
                                num = d;

                            }


                        } else {
                            Log.d("RETURN", "No-Response");
                            Message msg = Message.obtain(mHandler, MESSAGE_LOG);
                            msg.obj = "Response:" + "\n" + "No-Response" + "\n";
                            mHandler.sendMessage(msg);
                        }

//                    String sleep_command = "55AA17010000000000000000000000000000000000001701";
//                    send_command = hexStringToByteArray(sleep_command);        //Convert the string to the hex data
//                    mUart.write(send_command, send_command.length);       //Send the data over UART


                    } else {
                        Log.d("SEND", "ELSE CONDITION FINGERPRINT");
                        long start_time2 = System.currentTimeMillis();

                        long wait_time2;
                        //if for enrolling finger print
                        if (str.indexOf("55AA0301") != -1) {
                            wait_time2 = 6000;

                        } else {
                            wait_time2 = 2500;
                        }
                        //other than enrolling
//                    long wait_time2 = 6000;
                        long end_time2 = start_time2 + wait_time2;
//                    byte[] fp_buffer;
//                    String fp_str=str;
//                    if(fp_str.isEmpty()){
//                        Toast.makeText(MainActivity.this,R.string.error,Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                    fp_buffer = hexStringToByteArray(fp_str);
//
//
//                    int ret = mUart.write(fp_buffer, fp_buffer.length);       //Send the data over UART
//
//                    if(ret > 0)                                         /*Check if write was successful*/
//                    {
//
//                        Log.d("Error","Write Success");
//                    }
//                    else if(ret < 0)                                /*Write Fail, Print Error Message*/
//                    {
//
//                        Log.d("Error","Not Written the value");
//
//                    }
                        while (System.currentTimeMillis() < end_time2) {
                            int val = mUart.read(receive_buff, receive_buff.length); //Read Hex data from UART
//                        Log.d(INSTRUCTION, "Return value" + val);

                            final_hex_buff = byteArrayToHex(receive_buff);                          //Convert Hex data to String


                            //  if(final_hex_buff.indexOf(compare) != -1)///*val > 48 && */(final_hex_buff.charAt(0) == 'A' && final_hex_buff.charAt(1) == 'A' && final_hex_buff.charAt(2) == '5' && final_hex_buff.charAt(3) == '5'))                                    //half of 96,for enroll command which is send from command promt
                            //  {
                            for (int i = 0; i < 2 * val; i++)
                                packet_extract += final_hex_buff.charAt(i);

//                        if(final_hex_buff.indexOf(compare_identify) != -1)                             //if response for finger identity 0x0201
//                            break;
                            //       break;
                            //   }
//                        if(final_hex_fngr.indexOf(compare_identify) != -1)                     //if response for finger identity 0x0201
//                            break;
                        }

                        Log.d("RETURN", "From Command Line" + packet_extract);
                        int check_len = packet_extract.length();
                        Log.d("RETURN", "L-BYTE2 " + check_len / 2);
                        if (/*(check_len >= 48 )&&*/ check_len != 0)                                                     //when card present, the data which is received from UART will be more than 32 char
                        {
                            int a = 0, b = 0, n = 0;
                            for (a = 0; a < (check_len / 48); a++) {

                                for (b = (48 * a); b < 48 + n; b++)// for(b=(48*a); b< (b+48);b++)
                                {

                                    Response1 += packet_extract.charAt(b);
//                                Log.d("RETURN", "Response" + Response1);

                                }
                                String response = Response1.substring(12, 16);
                                Log.d(INSTRUCTION, Response1.toUpperCase());
                                Message msg = Message.obtain(mHandler, MESSAGE_LOG);
                                //                //Print the UID
                                if (Response1.indexOf(compare_identify) != -1 && Response1.indexOf("f4ff") == -1 && response.indexOf("0000") != -1) {
                                    for (int g = 16; g < 20; g++) {
                                        template_no += Response1.charAt(g);
                                    }
                                    Log.d(INSTRUCTION, "Template Number:" + template_no);
                                    msg.obj = "Response:" + (a + 1) + "\n" + Response1.toUpperCase() + "\n" + "Template no:" + template_no + "\n";
                                } else {

                                    msg.obj = "Response:" + (a + 1) + "\n" + Response1.toUpperCase() + "\n";
                                }
                                mHandler.sendMessage(msg);
                                Response1 = "";
                                n = b;
                            }
                        } else {
                            Log.d("Response:", "No-Response");
                            Message msg = Message.obtain(mHandler, MESSAGE_LOG);
                            //                //Print the UID
                            msg.obj = "Response:" + "\n" + "No-Response" + "\n";
                            mHandler.sendMessage(msg);
                        }
                    }

                    rfid_stop = false;
//                System.exit(0);
                    /*Close the app*/
                } else {
                    Log.d("RETURN", "No-Response");
                    Message msg = Message.obtain(mHandler, MESSAGE_LOG);
                    msg.obj = "Response:" + "\n" + "No-Response" + "\n";
                    mHandler.sendMessage(msg);


                }
            }
            /*Wait for 2Sec after reading for 2Sec, before closing the app*/
//            try {
//                mThread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }

        /*Close the app*/
//        finishAffinity();
//        System.exit(0);
        /*Close the app*/

    }


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_LOG:
                    //     mLog.setText(mLog.getText() + (String)msg.obj);
                    break;
            }
        }
    };

    private void deviceModeChangeAfterConnection(final String colorValue, final int timestamp, final String text) {

        new Thread() {
            public void run() {

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        int DEFAULT_PIN = 911;
                        int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
                        int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
                        int blue_pin = 69 + DEFAULT_PIN;
                        /*Define the Blue Pin number*/
                        /*Check for Valid conditions for the RGB LED*/

                        char red_value = colorValue.charAt(0);
                        char green_value = colorValue.charAt(1);
                        char blue_value = colorValue.charAt(2);

                        mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));

                        if (Util.checkInternet(MainActivity.this)) {
                            util.setStatus("R");
                        } else {
                            util.setStatus("N");
                        }

                        LCDText(text, util.getStatus());
                        try {
                            Thread.sleep(timestamp);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }.start();
    }


    private void deviceModeChange(final String colorValue, final int timestamp, final String text) {

        new Thread() {
            public void run() {

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        int DEFAULT_PIN = 911;
                        int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
                        int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
                        int blue_pin = 69 + DEFAULT_PIN;
                        /*Define the Blue Pin number*/
                        /*Check for Valid conditions for the RGB LED*/

                        char red_value = colorValue.charAt(0);
                        char green_value = colorValue.charAt(1);
                        char blue_value = colorValue.charAt(2);

                        mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));

                        LCDText(text, util.getStatus());
                        try {
                            Thread.sleep(timestamp);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (text.contains(Constants.MSG_RFID_MODE)) {

                            int speechStatus = textToSpeech.speak(Constants.SPEAK_RFID_MODE, TextToSpeech.QUEUE_FLUSH, null);
                            if (speechStatus == TextToSpeech.ERROR) {
                                Log.e("TTS", "Error in converting Text to Speech!");
                            }
                        } else {

                            int speechStatus = textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                            if (speechStatus == TextToSpeech.ERROR) {
                                Log.e("TTS", "Error in converting Text to Speech!");
                            }
                        }
                    }
                });
            }
        }.start();
    }


    private void changeColor(final String colorValue, final int timestamp, final String text) {

        new Thread() {
            public void run() {

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        int DEFAULT_PIN = 911;
                        int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
                        int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
                        int blue_pin = 69 + DEFAULT_PIN;
                        /*Define the Blue Pin number*/
                        /*Check for Valid conditions for the RGB LED*/

                        char red_value = colorValue.charAt(0);
                        char green_value = colorValue.charAt(1);
                        char blue_value = colorValue.charAt(2);

                        mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));

                        LCDText(text, util.getStatus());
                        try {
                            Thread.sleep(timestamp);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (!text.contains(Constants.DEFWELCOMETEXT)) {

                            if (text.contains(Constants.MSG_VALID_USER)) {
                                int speechStatus = textToSpeech.speak("Welcome " + NAME, TextToSpeech.QUEUE_FLUSH, null);
                                if (speechStatus == TextToSpeech.ERROR) {
                                    Log.e("TTS", "Error in converting Text to Speech!");
                                }
                            }
                            else if (text.contains(Constants.MSG_VALID_COLOR)) {
                                int speechStatus = textToSpeech.speak(Constants.MSG_VALID_COLOR + NAME, TextToSpeech.QUEUE_FLUSH, null);
                                if (speechStatus == TextToSpeech.ERROR) {
                                    Log.e("TTS", "Error in converting Text to Speech!");
                                }

                            } else {
                                int speechStatus = textToSpeech.speak(Constants.MSG_INVALID, TextToSpeech.QUEUE_FLUSH, null);
                                if (speechStatus == TextToSpeech.ERROR) {
                                    Log.e("TTS", "Error in converting Text to Speech!");
                                }

                            }
                        } else {

                            if (text.contains(Constants.MSG_WIFI_MODE)) {
                                int speechStatus = textToSpeech.speak(Constants.MSG_WIFI_MODE, TextToSpeech.QUEUE_FLUSH, null);
                                if (speechStatus == TextToSpeech.ERROR) {
                                    Log.e("TTS", "Error in converting Text to Speech!");
                                }
                            }
                        }

                        long unixTime = System.currentTimeMillis() / 1000L;

//                            if (mqttManager != null) {
//                                if (isScanned) {
//
//                                    JSONObject postData = new JSONObject();
//                                        try {
//                                            postData.put("time", unixTime);
//                                            postData.put("guid", Unique_ID.toUpperCase());
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                    pubSubDemo.publish("rinira/30/device/data/swipe/" + MAC_ADDRESS, postData.toString());
//
////                                    if (isSwipe) {
////
////                                        JSONObject postData = new JSONObject();
////                                        try {
////                                            postData.put("time", unixTime);
////                                            postData.put("guid", Unique_ID.toUpperCase());
////                                        } catch (JSONException e) {
////                                            e.printStackTrace();
////                                        }
////                                        Log.d("AWS", "Swipe");
////                                        pubSubDemo.publish("rinira/30/device/data/swipe/" + MAC_ADDRESS, postData.toString());
////                                    } else if(isPresence) {
////
////                                        JSONObject postData = new JSONObject();
////
////                                        try {
////                                            postData.put("time", unixTime);
////                                            postData.put("userId", "1545");
////                                        } catch (JSONException e) {
////                                            e.printStackTrace();
////                                        }
////                                        Log.d("AWS", "Presence");
////                                        pubSubDemo.publish("rinira/30/server/data/presence", postData.toString());
////                                    }
//                                    isScanned = false;
//                                    isSwipe = false;
//                                    isPresence = false;
//                                }
//                            }

                        isScanned = false;
                        isSwipe = false;
                        isPresence = false;
                    }
                });
            }
        }.start();
    }

    private void LCDText(String strLCD, final String status) {

        if(lcd_flag == true) {
            mI2C.IO_set(3, false);                                              /*Make LCD_RST high*/
            mI2C.IO_out(3, 1);

            mI2C.IO_set(2, false);                                              /*Enable GPS_EN pin of I/O expander*/
            mI2C.IO_out(2, 1);

            /*Initialise LCD*/

            String date = Util.getDate();
            String time = Util.getTime();

            siteCode = pref.getString("site_code", "");

//            if (!siteCode.equalsIgnoreCase("")) {
//                date_time = date + " " + time + " " + siteCode.toUpperCase() + " R";
//            } else {
//                date_time = date + " " + time + " " + "####" + " R";
//            }

            if (!siteCode.equalsIgnoreCase("")) {
                date_time = date + " " + time + " " + siteCode.toUpperCase() + " " + status;
            } else {
                date_time = date + " " + time + " " + "#### " + status;
            }

            mI2C.PutChar(0, row1, strLCD);

            if(strLCD.contains(Constants.MSG_WIFI_MODE)) {

                mI2C.PutChar(0, row2, "                ");
            }

            else if ((strLCD.contains(Constants.DEFWELCOMETEXT))  || (strLCD.contains(Constants.MSG_RFID_MODE)) ) {
                mI2C.PutChar(0, row2, date_time);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        String date = Util.getDate();
                        String time = Util.getTime();

                        if (!siteCode.equalsIgnoreCase("")) {
                            date_time = date + " " + time + " " + siteCode.toUpperCase() + " " + status;
                        } else {
                            date_time = date + " " + time + " " + "#### " + status;
                        }

                        handler.postDelayed(this, 60 * 1000);
                        mI2C.PutChar(0, row2, date_time);
                    }
                }, 00000);

            }
            else {

                if (!strLCD.contains(Constants.MSG_INVALID)) {
                    mI2C.PutChar(0, row2, trimText(NAME));
                } else{

                    mI2C.PutChar(0, row2, "                ");
                    try {
                        lcdThread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

//                try {
//                    Thread.sleep(4000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

//                try {
//                    lcdThread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
        }

    }

    private String trimText(String text) {

        int len = text.length();

        if (len == 16 || len > 16) {
            return text;
        } else {

            int spaceLen = 16 - len;
            String spaces = "";

            while (spaceLen > 0) {
                spaces += " ";
                spaceLen--;
            }
            Log.d("Space", text + spaces);
            return text + spaces;

        }
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private String convertToDecimal(String UUIDHex) {

        String splittedtext[] = splitByNumber(UUIDHex, 2);
        String newSplittedtext = "";

        for (int i = splittedtext.length-1; i==0; i--) {
            newSplittedtext  += newSplittedtext;
        }
        return newSplittedtext;
    }

    public static String[] splitByNumber(String str, int size) {
        return (size<1 || str==null) ? null : str.split("(?<=\\G.{"+size+"})");
    }

    //////////////Convert the hex value to String////////////////
    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    //////////////CRC Calculation FUNCTION/////////////////
    private static int CRC16_BUYPASS(String buffer){
        int strLen = buffer.length();
        int[] intArray;
        int crc = 0x0000;                       //initial value
        int polynomial=0x8005;                  //Polynomial value for BUYPASS CRC calculation

        //Convert the hex value to string for simplified calculations
        if (strLen % 2 != 0) {
            buffer = buffer.substring(0, strLen - 1) + "0"
                    + buffer.substring(strLen - 1, strLen);
            strLen++;
        }

        intArray= new int[strLen / 2];
        int ctr = 0;
        for (int n = 0; n < strLen; n += 2) {
            intArray[ctr] = Integer.valueOf(buffer.substring(n, n + 2), 16);
            ctr++;
        }
        //Calculate 16-bit CRC
        for (int b : intArray) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit)
                    crc ^= polynomial;
            }
        }
        crc &= 0xFFFF;

        return crc;
    }
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    // SendReceive

//    public class SendReceive extends Thread {
//
//        Socket socket;
//        InputStream inputStream;
//        OutputStream outputStream;
//
//        public SendReceive(Socket socket) {
//            this.socket = socket;
//
//            try {
//                inputStream = socket.getInputStream();
//                outputStream =socket.getOutputStream();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        public void run() {
//            super.run();
//
//            byte[] buffer = new byte[1024];
//            int bytes;
//
//            while (socket != null) {
//                try {
//                    bytes = inputStream.read(buffer);
//                    if (bytes > 0) {
//
//                        BluetoothHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }
//
//        public void write(byte[] bytes) {
//
//            try {
//                outputStream.write(bytes);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    // ServerClass

//    public class ServerClass extends Thread {
//
//        Socket socket;
//        ServerSocket serverSocket;
//
//        @Override
//        public void run() {
//            super.run();
//            try {
//                serverSocket = new ServerSocket(8888);
//                socket = serverSocket.accept();
//                try {
//                    socket.setReuseAddress(true);
//                    socket.setKeepAlive(true);
////                socket.setSoLinger(true, 3000);
//                } catch (SocketException e) {
//                    e.printStackTrace();
//                }
//                sendReceive = new SendReceive(socket);
//                util.setSendReceive(sendReceive);
//                sendReceive.start();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    //ClientClass

//    public class ClientClass extends Thread {
//
//        Socket socket;
//        String hostAdd;
//
//        public  ClientClass(InetAddress hostAddress) {
//            hostAdd = hostAddress.getHostAddress();
//            socket = new Socket();
//        }
//
//
//        @Override
//        public void run() {
//            super.run();
//
//            try {
//
//                socket.connect(new InetSocketAddress(hostAdd, 8888), 500);
//                sendReceive = new SendReceive(socket);
//                sendReceive.start();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }

    private void initArrays() {

        arrHrUser = new ArrayList<HRUser>(Arrays.asList(
                new HRUser("B65EEAFA","Adharma Swami"),
                new HRUser("86DC00FB","Atman Sibal"),

                new HRUser("3709E70F","Girish Thakur"),
                new HRUser("A63503FB","Dushkriti Madan"),

                new HRUser("2760E70F","Jambha Sami"),
                new HRUser("96E573FB","Kirsi Prashad"),
                new HRUser("F656FDFA","Malajit Sethi"),
                new HRUser("D690FFFA","Nirveli Pathak"),

                new HRUser("A6FBEBFA","Rahul Sharma"),
                new HRUser("06B9F1FA","Sany Shroff"),

                new HRUser("66A201FB","Trisna Chander"),
                new HRUser("1784E70F","Vasanti Sahni")));

        arrColorUser = new ArrayList<ColorUser>(Arrays.asList(
                new ColorUser("16AEEAFA","Blue",     "001"),
                new ColorUser("D6D9EEFA","Brown" ,   "000"),
                new ColorUser("465FEAFA","Gray",     "000"),
                new ColorUser("6672496D","Green",    "010"),
                new ColorUser("A666416D","Yellow",   "110"),
                new ColorUser("467A386D","Black",    "000"),
                new ColorUser("86D9446D","Turquoise","011"),
                new ColorUser("5660E76C","White",    "111"),
                new ColorUser("168A476D","Purple",   "101"),
                new ColorUser("0685F1FA","Orange",   "100"),
                new ColorUser("66A700FB","Pink",     "101"),
                new ColorUser("C610F3FA","Red",      "100"),
                new ColorUser("374CE40F","Blue",     "001"),
                new ColorUser("F76E1210","Brown" ,   "000"),
                new ColorUser("17AFE60F","Gray",     "000"),
                new ColorUser("C772EA0F","Green",    "010"),
                new ColorUser("B76EE80F","Yellow",   "110"),
                new ColorUser("07C3E60F","Black",    "000"),
                new ColorUser("770BE10F","Turquoise","011"),
                new ColorUser("A7D90A10","White",    "111"),
                new ColorUser("8665E96C","Purple",   "101"),
                new ColorUser("E645E06C","Orange",   "100"),
                new ColorUser("B7BB0B10","Pink",     "101"),
                new ColorUser("F775EC0F","Red",      "100")));
    }
}
