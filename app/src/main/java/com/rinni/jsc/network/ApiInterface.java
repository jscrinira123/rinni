package com.rinni.jsc.network;

import com.rinni.jsc.model.DeviceInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


//    URL : http://api.tml.com/v1/device/get.xml
//    Data : platformId=1&token=abc123&serial=a3.0b.00.00.d3.07

    @GET("device/get.json")
    Call<DeviceInfo> getDeviceInfo(@Query("platformId") String platformId, @Query("token") String token,
                                   @Query("serial") String serial);
}