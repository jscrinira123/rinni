package com.rinni.jsc.network;

/**
 * Created by riniramac001 on 28/12/16.
 */
import com.rinni.jsc.util.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NetworkUtil {

    private static Retrofit retrofit = null;

    public static Retrofit getClient(boolean isText) {

        if (retrofit==null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = TrustUnsafe.getUnsafeOkHttpClient();
            OkHttpClient.Builder builder = okHttpClient.newBuilder();

            builder.addInterceptor(logging);
            if (isText == true) {

                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.PRODUCTION_URL)
                        .client(builder.build())
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .build();
            } else {
                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.PRODUCTION_URL)
                        .client(builder.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
        return retrofit;
    }
}