package com.rinni.jsc.bluetooth;

public interface OnTestListener {

    public void onTestFinished(String name);
}
