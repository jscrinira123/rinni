package com.rinni.jsc.bluetooth;

public interface BluetoothResponse {

    public void onResponse(String response);
}
