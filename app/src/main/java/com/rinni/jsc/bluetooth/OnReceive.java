package com.rinni.jsc.bluetooth;

public class OnReceive {

    OnReceivedListener mConnectedListener;

    public OnReceive(OnReceivedListener connectedListener) {
        this.mConnectedListener = connectedListener;
    }

    public void OnReceive() {
        mConnectedListener.onReceived();
    }
}
