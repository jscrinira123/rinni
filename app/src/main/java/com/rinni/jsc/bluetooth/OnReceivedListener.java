package com.rinni.jsc.bluetooth;

public interface OnReceivedListener {

    public void onReceived();
}
