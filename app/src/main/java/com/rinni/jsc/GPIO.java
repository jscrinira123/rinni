package com.rinni.jsc;


/**
 * Serial Communication Management
 * the device name of first uart: /dev/ttymxc1
 * the device name of second uart: /dev/ttymxc2
 */  
public class GPIO {

	/**
	 * A native method that is implemented by the 'native-lib' native library,
	 */
	public native int getGpioStatus(int gpioId);
	/**
	 * A native method that is implemented by the 'native-lib' native library,
	 */
	public native int setGpioStatus(int gpioId,int status);
	/**
	 * A native method that is implemented by the 'native-lib' native library,
	 */
	public native int getAdcStatus();


}

