package com.rinni.jsc.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.rinni.jsc.GPIO;
import com.rinni.jsc.R;
import com.rinni.jsc.connections.ClientClass;
import com.rinni.jsc.connections.ServerClass;
import com.rinni.jsc.i2c.I2C;
import com.rinni.jsc.util.Constants;
import com.rinni.jsc.util.I2cInstance;
import com.rinni.jsc.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WifiActivity extends AppCompatActivity {

    WifiManager mWifiManager;

    WifiP2pManager mP2PManager;
    WifiP2pManager.Channel mP2PChannel;
    BroadcastReceiver mP2PReceiver;
    IntentFilter mP2PIntentFilter;

    String TAG = WifiActivity.class.getSimpleName();

    List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    String deivceNameArray[];
    WifiP2pDevice deviceArray[];

    static final int MESSAGE_READ = 1;

    //Instances

    ServerClass serverClass;
    ClientClass clientClass;
    static SendReceive sendReceive;

    static String msg;

    //LCD

    private I2C mI2C;

    private String strLCD="";
    private static boolean lcd_flag;
    private boolean io_exp_off = false;
    private int fdLCD;
    String line1 = "";
    String line2 = "";
    int row1 = 0;
    int row2 = 1;
    private Thread lcdThread;
    String siteCode;
    SharedPreferences pref;
    String date_time;
    String NAME = "";

    // RFID

    GPIO mgpio;
    TextToSpeech textToSpeech;

    //WiFi List

    private final List<Integer> blockedKeys = new ArrayList<>(Arrays.asList(KeyEvent.KEYCODE_POWER));


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
            Log.d("WiFi", "Long Pressed");

            // initialize P2P on long press.
            registerReceiver(mP2PReceiver, mP2PIntentFilter);
            discoverP2PNetworks();
        }
    }

    private void LCDText(String strLCD) {

        if(lcd_flag == true) {
            Log.d("IO_EXP", "LCD SET");
            mI2C.IO_set(3, false);                                              /*Make LCD_RST high*/
            mI2C.IO_out(3, 1);

            mI2C.IO_set(2, false);                                              /*Enable GPS_EN pin of I/O expander*/
            mI2C.IO_out(2, 1);

            /*Initialise LCD*/

            String date = Util.getDate();
            String time = Util.getTime();

            siteCode = pref.getString("site_code", "");

//            if (!siteCode.equalsIgnoreCase("")) {
//                date_time = date + " " + time + " " + siteCode.toUpperCase() + " R";
//            } else {
//                date_time = date + " " + time + " " + "####" + " R";
//            }

            if (!siteCode.equalsIgnoreCase("")) {
                date_time = date + " " + time + " " + siteCode.toUpperCase() + " R";
            } else {
                date_time = date + " " + time + " " + "#### R";
            }

            mI2C.PutChar(0, row1, strLCD);

            if (strLCD.contains(Constants.DEFWELCOMETEXT)) {
                mI2C.PutChar(0, row2, date_time);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        String date = Util.getDate();
                        String time = Util.getTime();

                        if (!siteCode.equalsIgnoreCase("")) {
                            date_time = date + " " + time + " " + siteCode.toUpperCase() + " R";
                        } else {
                            date_time = date + " " + time + " " + "#### R";
                        }

                        handler.postDelayed(this, 60 * 1000);
                        mI2C.PutChar(0, row2, date_time);
                    }
                }, 00000);

            }

            else {

                if (!strLCD.contains(Constants.MSG_INVALID)) {
                    mI2C.PutChar(0, row2, trimText(NAME));
                } else{

                    mI2C.PutChar(0, row2, "                ");
                    try {
                        lcdThread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private String trimText(String text) {

        int len = text.length();

        if (len == 16 || len > 16) {
            return text;
        } else {

            int spaceLen = 16 - len;
            String spaces = "";

            while (spaceLen > 0) {
                spaces += " ";
                spaceLen--;
            }
            Log.d("Space", text + spaces);
            return text + spaces;

        }
    }

    private void initRFID() {

        pref = this.getSharedPreferences("JSC", 0);
        mgpio= new GPIO();
        textToSpeech = Util.getTTSInstance(this);
        // LCD
        strLCD = "";                                                 /*If any extra other than label "string" or related to io expander is found, print the default text on LCD*/
        lcd_flag = true;
        io_exp_off = false;

        mI2C = I2cInstance.getInstance();
        LCDText(Constants.DEFWELCOMETEXT);

        /*Start the thread for receiving*/
//        mThread = new Thread(MainActivity.this);
//        mThread.start();
        /*Start the thread for receiving*/
    }

    private void deviceInWiFi(final String colorValue, final int timestamp, final String text) {

        new Thread() {
            public void run() {

                WifiActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        int DEFAULT_PIN = 911;
                        int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
                        int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
                        int blue_pin = 69 + DEFAULT_PIN;
                        /*Define the Blue Pin number*/
                        /*Check for Valid conditions for the RGB LED*/

                        char red_value = colorValue.charAt(0);
                        char green_value = colorValue.charAt(1);
                        char blue_value = colorValue.charAt(2);

                        mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
                        mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));

                        LCDText(text);
                        try {
                            Thread.sleep(timestamp);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (text.contains(Constants.MSG_WIFI_MODE)) {
                            int speechStatus = textToSpeech.speak(Constants.MSG_WIFI_MODE, TextToSpeech.QUEUE_FLUSH, null);
                            if (speechStatus == TextToSpeech.ERROR) {
                                Log.e("TTS", "Error in converting Text to Speech!");
                            }
                        }
                    }
                });
            }
        }.start();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_wifi);

        mgpio= new GPIO();
        textToSpeech = Util.getTTSInstance(this);
        // LCD
        strLCD = "";                                                 /*If any extra other than label "string" or related to io expander is found, print the default text on LCD*/
        lcd_flag = true;
        initNetworks();
        initRFID();

    }

    private void initNetworks() {

        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mP2PManager = (WifiP2pManager) getApplicationContext().getSystemService(Context.WIFI_P2P_SERVICE);
        mP2PChannel = mP2PManager.initialize(this, getMainLooper(), null);

        List<WifiConfiguration> list = mWifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            mWifiManager.removeNetwork(i.networkId);
            //wifiManager.saveConfiguration();
        }

        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }

        mP2PReceiver = new WiFiDirectBroadcastReceiver(mP2PManager, mP2PChannel, WifiActivity.this);
        mP2PIntentFilter = new IntentFilter();

        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {

            switch (msg.what) {

                case MESSAGE_READ :
                    byte[]  readBuff = (byte[]) msg.obj;

                    String tmpMsg = new String(readBuff, 0, msg.arg1);

                  //  disConnect(tmpMsg);

                    connectWiFi(tmpMsg);
                    Log.d("Read Msg", "Msg "+ tmpMsg);


                    break;
            }
            return true;
        }
    });



    @Override
    protected void onResume() {
        super.onResume();
    }

    private void connectWiFi(final String response) {

        String SSID = response.split(",")[0].trim();

        String passKey = response.split(",")[1].trim();

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", SSID);
        wifiConfig.preSharedKey = String.format("\"%s\"", passKey);
        int netId = mWifiManager.addNetwork(wifiConfig);
        mWifiManager.disconnect();
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.reconnect();

    }

    private void disConnect() {

        mP2PManager.removeGroup(mP2PChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onFailure(int reasonCode) {
                Log.d(TAG, "Disconnect failed. Reason :" + reasonCode);

            }
            @Override
            public void onSuccess() {
                Log.d(TAG, "Disconnect success. Reason :");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mP2PReceiver != null) {
            unregisterReceiver(mP2PReceiver);
        }
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(mReceiver);
//        unregisterReceiver(mWifiScanReceiver);
//    }


    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {

            Log.d("PeerList", "List : " + peerList);

            if (!peerList.getDeviceList().equals(peers)) {
                peers.clear();
                peers.addAll(peerList.getDeviceList());
                deivceNameArray = new String[peerList.getDeviceList().size()];
                deviceArray = new WifiP2pDevice[peerList.getDeviceList().size()];
                int index = 0;

                for (WifiP2pDevice device : peerList.getDeviceList()) {
                    deivceNameArray[index] = device.deviceName;
                    deviceArray[index] = device;
                    index++;
                }
                // set Adapter
            }

            if (peers.size() == 0) {
                Log.d(WifiActivity.class.getSimpleName(), "No Device Found");
            }

        }
    };

    WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {


            final InetAddress groupOwner = wifiP2pInfo.groupOwnerAddress;

            if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {

                Log.d(TAG, "Host");
                serverClass = new ServerClass(WifiActivity.this);
                serverClass.start();

            } else  if(wifiP2pInfo.groupFormed) {

                Log.d(TAG, "Client");
                clientClass = new ClientClass(WifiActivity.this, groupOwner);
                clientClass.start();
            }
        }
    };

    private void discoverP2PNetworks() {

        deviceInWiFi("110", 1000, Constants.MSG_WIFI_MODE);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WifiP2pManager.ActionListener actionListener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int i) {

                Log.d(TAG, "Discovery Failed");
            }
        };

        mP2PManager.removeGroup(mP2PChannel, actionListener);
        mP2PManager.discoverPeers(mP2PChannel, actionListener);

    }


    // SendReceive

    public class SendReceive extends Thread {

        Socket socket;
        InputStream inputStream;
        OutputStream outputStream;

        public SendReceive(Socket socket) {
            this.socket = socket;

            try {
                inputStream = socket.getInputStream();
                outputStream =socket.getOutputStream();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            super.run();

            byte[] buffer = new byte[1024];
            int bytes;

            while (socket != null) {
                try {
                    bytes = inputStream.read(buffer);
                    if (bytes > 0) {

                        handler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

        public void write(byte[] bytes) {

            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    // ServerClass

//    public class ServerClass extends Thread {
//
//        Socket socket;
//        ServerSocket serverSocket;
//
//        @Override
//        public void run() {
//            super.run();
//            try {
//                serverSocket = new ServerSocket(8888);
//                socket = serverSocket.accept();
//                sendReceive = new SendReceive(socket);
//                sendReceive.start();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//
//    }
//    //ClientClass
//
//    public class ClientClass extends Thread {
//
//        Socket socket;
//        String hostAdd;
//
//        public  ClientClass(InetAddress hostAddress) {
//            hostAdd = hostAddress.getHostAddress();
//            socket = new Socket();
//        }
//
//
//        @Override
//        public void run() {
//            super.run();
//
//            try {
//                socket.connect(new InetSocketAddress(hostAdd, 8888), 500);
//                sendReceive = new SendReceive(socket);
//                sendReceive.start();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }
//
//    public static class FileServerAsyncTask extends AsyncTask<Void, Void, String> {
//
//        private Context context;
//        private TextView statusText;
//
//        /**
//         * @param context
//         */
//        public FileServerAsyncTask(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        protected String doInBackground(Void... params) {
//
//
//                sendReceive.write(msg.getBytes());
//                return "";
//
//        }
//
//        /*
//         * (non-Javadoc)
//         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
//         */
//        @Override
//        protected void onPostExecute(String result) {
//            if (result != null) {
//
//            }
//
//        }
//
//        /*
//         * (non-Javadoc)
//         * @see android.os.AsyncTask#onPreExecute()
//         */
//        @Override
//        protected void onPreExecute() {
//
//        }
//
//    }
}
