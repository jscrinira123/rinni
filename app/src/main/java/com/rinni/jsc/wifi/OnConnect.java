package com.rinni.jsc.wifi;

public class OnConnect {

    OnConnectedListener mConnectedListener;

    public OnConnect(OnConnectedListener connectedListener) {
        this.mConnectedListener = connectedListener;
    }

    public void onConnect() {
        mConnectedListener.onConnected();
    }
}
