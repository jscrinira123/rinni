package com.rinni.jsc.wifi;

public interface OnConnectedListener {

    public void onConnected();
}
