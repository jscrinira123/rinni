package com.rinni.jsc.wifi.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rinni.jsc.R;
import java.util.List;


public class WiFiDeviceListAdapter extends ArrayAdapter<ScanResult> {

    private LayoutInflater mLayoutInflater;
    private List<ScanResult> mDevices;
    private int  mViewResourceId;

    public WiFiDeviceListAdapter(Context context, int tvResourceId, List<ScanResult> devices){
        super(context, tvResourceId,devices);
        this.mDevices = devices;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = tvResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(mViewResourceId, null);



        ScanResult device = mDevices.get(position);

        if (device != null) {
            TextView deviceName = (TextView) convertView.findViewById(R.id.tvDeviceName);
            TextView deviceAdress = (TextView) convertView.findViewById(R.id.tvDeviceAddress);

            if (deviceName != null) {
                deviceName.setText(device.SSID);
            }
            if (deviceAdress != null) {
                deviceAdress.setText(device.BSSID);
            }
        }

        return convertView;
    }

}
