package com.rinni.jsc.connections;

import android.content.Context;
import android.util.Log;

import com.rinni.jsc.uart.MainActivity;
import com.rinni.jsc.util.JSCApp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class ServerClass extends Thread {

    Socket socket;
    ServerSocket serverSocket;
    Context mContext;
    String TAG = MainActivity.class.getSimpleName();

    public ServerClass(Context context) {
        this.mContext = context;
        Log.d(TAG, "ServerClass 2" );
    }

    @Override
    public void run() {
        super.run();
        try {
            Log.d(TAG, "ServerClass 4" );
            serverSocket = new ServerSocket(8888);
            Log.d(TAG, "ServerClass 5" );
            socket = serverSocket.accept();
            try {
                socket.setReuseAddress(true);
//            socket.setKeepAlive(true);
                Log.d(TAG, "ServerClass 5.1" );
            } catch (SocketException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "ServerClass 6" );
            SendReceive sendReceive = new SendReceive(mContext, socket);
            JSCApp jscApp = (JSCApp) mContext.getApplicationContext();
            jscApp.setSendReceive(sendReceive);
            Log.d(TAG, "ServerClass 7" );
            sendReceive.start();
            Log.d(TAG, "ServerClass 8" );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
