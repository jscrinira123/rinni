package com.rinni.jsc.connections;

import android.content.Context;
import android.util.Log;

import com.rinni.jsc.uart.MainActivity;
import com.rinni.jsc.util.JSCApp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

public class ClientClass extends Thread {

    Socket socket;
    String hostAdd;
    Context mContext;

    String TAG = MainActivity.class.getSimpleName();

    public  ClientClass(Context context, InetAddress hostAddress) {

        Log.d(TAG, "ClientClass 2" );
        this.mContext = context;
        hostAdd = hostAddress.getHostAddress();

        Log.d(TAG, "ClientClass 3" );
        socket = new Socket();
        Log.d(TAG, "ClientClass 4" );
        try {
            socket.setReuseAddress(true);
//            socket.setKeepAlive(true);
            Log.d(TAG, "ClientClass 5" );
        } catch (SocketException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "ClientClass 6" );
    }

    @Override
    public void run() {
        super.run();

        Log.d(TAG, "ClientClass 8" );
        try {
            socket.connect(new InetSocketAddress(hostAdd, 8888), 500);
            SendReceive sendReceive = new SendReceive(mContext, socket);

            JSCApp jscApp = (JSCApp) mContext.getApplicationContext();
            jscApp.setSendReceive(sendReceive);

            Log.d(TAG, "ClientClass 9" );
            sendReceive.start();

            Log.d(TAG, "ClientClass 10" );
            // sendReceive.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}