package com.rinni.jsc.connections;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.rinni.jsc.util.JSCApp;


public class FileServerAsyncTask extends AsyncTask<String, Void, String> {

    private Context context;
    JSCApp jscApp;

    /**
     * @param context
     */
    public FileServerAsyncTask(Context context) {
        this.context = context;
        jscApp = (JSCApp) context.getApplicationContext();
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... strings) {

        String msg = strings[0];
        jscApp.getSendReceive().write(msg.getBytes());
        return "";
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String result) {

        if (result != null) {

            Toast.makeText(context, "Bluetooth successful", Toast.LENGTH_LONG).show();
        }
    }
}
