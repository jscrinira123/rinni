package com.rinni.jsc.connections;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.rinni.jsc.uart.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class SendReceive extends Thread {

    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    static final int MESSAGE_READ = 1;
    Context mContext;

    public static final String FILTER_ACTION = "com.rinira.justschedule.RESPONSE";

    public SendReceive() {
    }

    public SendReceive(Context context, Socket socket) {
        this.mContext = context;
        this.socket = socket;
        try {
            inputStream = socket.getInputStream();
            outputStream =socket.getOutputStream();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();

        byte[] buffer = new byte[1024];
        int bytes;

        while (socket != null) {
            try {
                bytes = inputStream.read(buffer);
                if (bytes > 0) {

                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void write(byte[] bytes) {

        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onResponseReceived(String data) {
        Intent intent = new Intent();
        intent.setAction(FILTER_ACTION);
        intent.putExtra("data", data);
        mContext.sendBroadcast(intent);
    }


    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message message) {
            switch (message.what) {

                case MESSAGE_READ :
                    byte[]  readBuff = (byte[]) message.obj;

                    String receivedMsg = new String(readBuff, 0, message.arg1);
                    Log.d("Read Msg", "Msg "+ receivedMsg);
                    onResponseReceived(receivedMsg);
                    break;
            }
        }
    };
}
