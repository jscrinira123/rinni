package com.rinni.jsc.util;

import android.util.Log;
import android.widget.Toast;

import com.rinni.jsc.BootActivity;
import com.rinni.jsc.i2c.I2C;

public class I2cInstance {

   private static I2C mI2C;

public static I2C getInstance()
{
    if (mI2C == null) {
       try {
          mI2C = new I2C();
          mI2C.open("/dev/i2c-1");
          mI2C.LCDInit();
       }
       catch(Exception e) {
          Log.d("I2C", "Initalzation falied");
          return null;
       }
    }
   return mI2C;
   }
}