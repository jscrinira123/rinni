package com.rinni.jsc.util;

import android.util.Log;

import com.rinni.jsc.i2c.I2C;

public class HRUser {

   public String id;
   public String name;

   public String colorName;
   public String colorCode;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }


   public HRUser(String id, String name) {
      this.id = id;
      this.name = name;

   }

   public HRUser(String id, String colorName, String colorCode) {

      this.id = id;
      this.colorName = colorName;
      this.colorCode = colorCode;
   }
}