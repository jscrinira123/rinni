package com.rinni.jsc.util;

import android.util.Log;

import com.rinni.jsc.i2c.I2C;
import com.rinni.jsc.uart.Uart;

public class UartInstance {

   private static Uart mUart;

public static Uart getInstance()
{
    if (mUart == null) {
       try {
          mUart = new Uart();
          mUart.open("/dev/ttyHSL1");
          mUart.setSerialPortParams(115200,8,1,'n');
       }
       catch(Exception e) {
          Log.d("I2C", "Initalzation falied");
          return null;
       }
    }
   return mUart;
   }
}