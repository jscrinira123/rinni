package com.rinni.jsc.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import com.rinni.jsc.connections.SendReceive;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {

    SendReceive sendReceive;

    public Util() {
    }

    public SendReceive getSendReceive() {
        return sendReceive;
    }

    public void setSendReceive(SendReceive sendReceive) {
        this.sendReceive = sendReceive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;

    private static TextToSpeech textToSpeech;

    public static String getDate() {

        String currentDate = new SimpleDateFormat("EEE", Locale.getDefault()).format(new Date());
        return currentDate.toUpperCase();
    }

    public static String getTime() {

        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        return currentTime;
    }

    public static TextToSpeech getTTSInstance(final Context context) {

        if (textToSpeech == null) {
            textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {

                        int ttsLang = textToSpeech.setLanguage(Locale.US);

                        if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                                || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "The Language is not supported!");
                        } else {
                            Log.i("TTS", "Language Supported.");
                        }
                        Log.i("TTS", "Initialization success.");
                    } else {
                        Toast.makeText(context, "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return textToSpeech;
    }

    public static String convertToDecimal(String UUIDHex) {

        String splittedtext1[] = splitByNumber(UUIDHex, 2);
        String splittedtext2[] = splitByNumber(splittedtext1[1], 2);
        String splittedtext3[] = splitByNumber(splittedtext2[1], 2);

        String hexVal = splittedtext3[1] + splittedtext3[0] + splittedtext2[0] + splittedtext1[0];

        long lngDecimal = hexToLong(hexVal);
        String strDecimal = Long.toString(lngDecimal);
        Log.d("decimal", "Hex is : " + hexVal);
        Log.d("decimal", "Decimal is : " + strDecimal);
        return strDecimal;
    }

    public static String getMACAddress(Context mContext) {

//        WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
//        WifiInfo info = manager.getConnectionInfo();
//        return info.getMacAddress();

        String android_id = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("DEVICE_ID", android_id);

        return android_id;

    }

    public static boolean checkInternet(Activity mActivity) {
        ConnectivityManager cm = (ConnectivityManager) mActivity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static long hexToLong(String hex) {
        return Long.parseLong(hex, 16);
    }

    public static String[] splitByNumber(String str, int size) {
        return (size<1 || str==null) ? null : str.split("(?<=\\G.{"+size+"})");
    }

}
