package com.rinni.jsc.util;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;

public class Constants extends Application {


    public  static String DEFWELCOMETEXT = "JSC WELCOMES YOU";
    public  static String TXT_CONFIGURE_WIFI = "Configure WiFi";

    public  static String DEFWELCOMETEXT_SPEAK = "J S C WELCOMES YOU";
    public  static String SPEAK_WIFI_CONNECTED = "Device is connected to wifi and ready to use";
    public  static String SPEAK_WIFI_NOT_CONNECTED = "Wifi is not connected please use jsc app to setup device";

    public  static String SPEAK_WIFI_CONNECTED_SUCCESS = "device connected to wifi successfully";
    public  static String SPEAK_BLUETOOTH_CONNECTED_SUCCESS = "jsc device paired to bluetooth device successfully";

    public  static String SPEAK_WIFI_DIRECT = "Wifi direct connection successful";

    public  static String MSG_VALID_USER = "Welcome User    ";
    public  static String MSG_VALID_COLOR = "Selected Color    ";
    public  static String MSG_WIFI_MODE = "Configure mode  ";

    public  static String MSG_RFID_MODE = "RFID mode on    ";

    public  static String SPEAK_RFID_MODE = "R F I D mode on";

    public  static String SPEAK_NO_PAIRED_LIST = "No paired list available";

    public  static String MSG_INVALID = "Invalid User    ";

    public  static String PRODUCTION_URL = "http://api.tml.rinira.in/v1/";

    public  static String PLATFORM_ID = "1";

    public  static String TOKEN = "abcxyz04bb6b44747c4100b9";

}