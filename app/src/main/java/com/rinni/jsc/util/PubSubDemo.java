package com.rinni.jsc.util;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

public class PubSubDemo {

    private static final String CUSTOMER_SPECIFIC_IOT_ENDPOINT = "a5v3ij952nla-ats.iot.us-east-2.amazonaws.com";

    AWSIotMqttManager mqttManager;
    String clientId;
    Context mContext;

    public PubSubDemo(Context context) {
    }

    public AWSIotMqttManager getMQTTInstance(Context context) {

        if (mqttManager == null) {

            this.mContext = context;
            clientId = UUID.randomUUID().toString();
            final CountDownLatch latch = new CountDownLatch(1);
            AWSMobileClient.getInstance().initialize(
                    mContext,
                    new Callback<UserStateDetails>() {
                        @Override
                        public void onResult(UserStateDetails result) {
                            latch.countDown();
                        }
                        @Override
                        public void onError(Exception e) {
                            latch.countDown();
                            Log.d("AWS Demo", "onError: ", e);
                        }
                    }
            );
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mqttManager = new AWSIotMqttManager(clientId, CUSTOMER_SPECIFIC_IOT_ENDPOINT);
        }

        return mqttManager;
    }

    public void connect() {

        Log.d("AWS Demo", "clientId = " + clientId);

        try {
            mqttManager.connect(AWSMobileClient.getInstance(), new AWSIotMqttClientStatusCallback() {
                @Override
                public void onStatusChanged(final AWSIotMqttClientStatus status,
                                            final Throwable throwable) {
                    Log.d("AWS Demo", "Status = " + String.valueOf(status));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                         //   tvStatus.setText(status.toString());

                            Log.d("AWS Demo", "Connection " + status.toString());
                            if (throwable != null) {
                                Log.d("AWS Demo", "Connection error.", throwable);
                            }
                        }
                    });
                }
            });
        } catch (final Exception e) {
            Log.d("AWS Demo", "Connection error.", e);
         //   tvStatus.setText("Error! " + e.getMessage());
        }
    }

    public void publish(String topic, String msg) {

//        final String topic = txtTopic.getText().toString();
//        final String msg = txtMessage.getText().toString();

//        final String topic = "JSC";
//        final String msg = "Welcome to JSC";

        try {
            mqttManager.publishString(msg, topic, AWSIotMqttQos.QOS0);
            Log.d("AWS Demo", "Publish Success.");

        } catch (Exception e) {
            Log.d("AWS Demo", "Publish error.", e);
        }
    }

    public void disconnect() {
        try {
            mqttManager.disconnect();
        } catch (Exception e) {
            Log.d("AWS Demo", "Disconnect error.", e);
        }
    }
}
