package com.rinni.jsc.util;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;

import com.rinni.jsc.connections.SendReceive;


public class JSCApp extends Application {

    private SendReceive sendReceive;

    public SendReceive getSendReceive() {
        return sendReceive;
    }

    public void setSendReceive(SendReceive sendReceive) {
        this.sendReceive = sendReceive;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //   ACRA.init(this);
    }
}