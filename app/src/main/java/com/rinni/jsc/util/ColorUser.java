package com.rinni.jsc.util;

public class ColorUser {

   public String id;
   public String name;
   public String code;


   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getCode() {
      return code;
   }

   public void setCode(String code) {
      this.code = code;
   }

   public ColorUser(String id, String name, String code) {
      this.id = id;
      this.name = name;
      this.code = code;
   }


}