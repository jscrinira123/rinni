package com.rinni.jsc;

/**
 * Created by Andrew on 2017/6/1.
 */

public class Tool {
    public static String getvalue_str(String strs){
        String str = null;
        switch(strs){
            case "GPIO_T1":
                str = "925";
                break;
            case  "GPIO_T2":
                str = "979";
                break;
            case  "GPIO_T3":
                str = "980";
                break;
            case "GPIO_T4":
                str = "947";
                break;
            case "GPIO_65":
                str = "976";
                break;
            case "GPIO_89":
                str = "1000";
                break;
            case "direction":
                str = "direction";
                break;
            case "value":
                str = "value";
                break;
            default:
                str="error";
                break;
        }
        return str;
    }

    public static String getValues(int num){
        String str=null;
        switch (num){
            case -1:
                //str = "Operation not permitted";
                str = "Error:value read numerical failure";
                break;

            default:
                str = num+"";
                break;
        }
        return str;
    }

    public static String getIndValue(int num){
        String str=null;
        switch (num){
            case -1:
                //str = "Operation not permitted";
                str = "Error:failure!";
                break;
            case 0:
                //str = "Operation not permitted";
                str = "Write value success!";
                break;

            default:
                str = num+"";
                break;
        }
        return str;
    }

    public static String getAdcValue(int num){
        String str=null;
        switch (num){
            case -1:
                str = "Operation not permitted";
                //str = "Error:failure!";
                break;

            default:
                str = num+"";
                break;
        }
        return str;
    }
}
