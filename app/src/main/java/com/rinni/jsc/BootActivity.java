package com.rinni.jsc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.rinni.jsc.i2c.I2C;
import com.rinni.jsc.model.DeviceInfo;
import com.rinni.jsc.network.ApiInterface;
import com.rinni.jsc.network.NetworkUtil;
import com.rinni.jsc.uart.MainActivity;
import com.rinni.jsc.util.Constants;
import com.rinni.jsc.util.I2cInstance;
import com.rinni.jsc.util.Util;
import com.rinni.jsc.wifi.WifiActivity;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BootActivity extends AppCompatActivity {

    private TextToSpeech textToSpeech;
    HashMap<String, String> map = new HashMap<String, String>();

    // RGB
    private Thread gpio_thread;
    private boolean update_flag = false;
    private int DEFAULT_PIN = 911;
    private String io_pin;
    private String io_dir;
    private String io_value;
    private char red_value;
    private char green_value;
    private char blue_value;
    GPIO mgpio;

    // LCD
    private I2C mI2C;
    private String strLCD="";
    private static boolean lcd_flag;
    private boolean io_exp_off=false;
    private int fdCLD;
    String line1 = "";                                                                   /*Initialize the String to store the value for Line1 Lcd Message*/
    String line2 = "";                                                                   /*Initialize the String to store the value for Line2 Lcd Message*/
    int row1 = 0;
    int row2 = 1;
    private Thread lcdThread;

    String DEVICE_ID;

    SharedPreferences pref;

    String SITE_ID = "";
    String SITE_CODE = "";
    String date_time;

    String TAG = BootActivity.class.getSimpleName();

    Button btnClick;

    LinearLayout activity_main;
    Util util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_i2c);

        pref = this.getSharedPreferences("JSC", 0);
        final String siteId = pref.getString("site_id", "");
        final String siteCode = pref.getString("site_code", "");

        util = new Util();

        if (Util.checkInternet(this)) {
            util.setStatus("R");
        } else {
            util.setStatus("N");
        }

//        activity_main = (LinearLayout)findViewById(R.id.activity_main);
//
//        activity_main.performClick();
//
//        activity_main.setPressed(true);
//        activity_main.invalidate();
//        // delay completion till animation completes
//        activity_main.postDelayed(new Runnable() {  //delay button
//            public void run() {
//                activity_main.setPressed(false);
//                activity_main.invalidate();
//                Log.d("Button", "Button clicked");
//                //any other associated action
//            }
//        }, 200);  // .8secs delay time

        mgpio = new GPIO();
        textToSpeech = Util.getTTSInstance(this);
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "JSC");

        strLCD = Constants.DEFWELCOMETEXT;
        lcd_flag = true;
        io_exp_off = false;

        mI2C = I2cInstance.getInstance();

        if(lcd_flag == true) {
            Log.d("IO_EXP","LCD SET");
            mI2C.IO_set(3,false);
            mI2C.IO_out(3, 1);

            String date= Util.getDate();
            String time = Util.getTime();

            if (Util.checkInternet(this)) {

                if (!siteCode.equalsIgnoreCase("")) {
                    date_time = date + " " + time + " " + siteCode.toUpperCase() + " " +util.getStatus();
                } else {
                    date_time = date + " " + time + " " + "#### " + util.getStatus();
                }
            } else {

                if (!siteCode.equalsIgnoreCase("")) {
                    date_time = date + " " + time + " " + siteCode.toUpperCase() + " " + util.getStatus();
                } else {
                    date_time = date + " " + time + " " + "#### " + util.getStatus();
                }
            }

            mI2C.IO_set(2,false);
            mI2C.IO_out(2, 1);
            int len = strLCD.length();
            if(len >= 32) {
                len = 32;
            }
            if(len > 16) {

                /*Extract the string to be printed on 1st line and 2nd line of the LCD*/
                for(int k=0; k<16; k++) {
                    line1 += strLCD.charAt(k);
                }
                for(int l=16;l<len;l++) {
                    line2 += strLCD.charAt(l);
                }
                /*Display the data on 1st line of the LCD*/
                mI2C.PutChar(0,row1,Constants.DEFWELCOMETEXT);

//                try {
//                    lcdThread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                /*Display the data on 2nd line of the LCD*/
                if (util.getStatus().equalsIgnoreCase("N")) {
                    mI2C.PutChar(0,row2,Constants.TXT_CONFIGURE_WIFI);

                } else {
                    mI2C.PutChar(0,row2,date_time);
                }
//                mI2C.PutChar(0,row2,date_time);

//                try {
//                    lcdThread.sleep(500);
//
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
            else {
                /*Display the data received from command line*/
                mI2C.PutChar(0,row1,Constants.DEFWELCOMETEXT);
                if (util.getStatus().equalsIgnoreCase("N")) {
                    mI2C.PutChar(0,row2,Constants.TXT_CONFIGURE_WIFI);

                } else {
                    mI2C.PutChar(0,row2,date_time);
                }


//                try {
//                    lcdThread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        }

        update_flag = true;
        io_dir = "o";
        io_value = "001";
      //  initDevice(io_value, defaultText);

        Log.d(TAG, "Step1");

//        if (Util.checkInternet(this)) {
//
//            if (!siteId.equalsIgnoreCase("") && !siteCode.equalsIgnoreCase("")) {
//
//                initRGB();
//                initSpeaker();
//            } else {
//
//                initDeviceInfo();
//            }
//        } else {
//
//            initRGB();
//            initSpeaker();
//        }

        Log.d(TAG, "Step2");

        initRGB();
        initSpeaker();

    }

    private void initDeviceInfo() {

//    Data : platformId=1&token=abc123&serial=a3.0b.00.00.d3.07

        DEVICE_ID = Util.getMACAddress(this);

        Log.d("DEVICE_ID", DEVICE_ID);

        ApiInterface apiService = NetworkUtil.getClient(false).create(ApiInterface.class);
        Call<DeviceInfo> callDeviceInfo = apiService.getDeviceInfo(Constants.PLATFORM_ID, Constants.TOKEN, DEVICE_ID);

        callDeviceInfo.enqueue(new Callback<DeviceInfo>() {
            @Override
            public void onResponse(Call<DeviceInfo> call, Response<DeviceInfo> response) {

                String successCode = String.valueOf(response.body().getCode());
                if (successCode.equalsIgnoreCase("0")) {

                    Log.d("Response", response.body().toString());
                    SITE_ID = String.valueOf(response.body().getDevice().getId());
                    SITE_CODE = response.body().getDevice().getCode();
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("site_id", SITE_ID);
                    editor.putString("site_code", SITE_CODE);
                    editor.commit();
                    initRGB();
                    initSpeaker();
                }
            }
            @Override
            public void onFailure(Call<DeviceInfo> call, Throwable t) {

            }
        });

    }

    private void initRGB() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(update_flag) {

                    int red_pin= 89 + DEFAULT_PIN;
                    int green_pin = 68 + DEFAULT_PIN;
                    int blue_pin = 69 + DEFAULT_PIN;

                        red_value = io_value.charAt(0);
                        green_value = io_value.charAt(1);
                        blue_value = io_value.charAt(2);
                        if(io_dir.equals("o")) {
                            mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));
                            mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));
                            mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));
                        }
                }
            }
        }, 200);
    }

    private void initSpeaker() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                int speechStatus = textToSpeech.speak(Constants.DEFWELCOMETEXT_SPEAK, TextToSpeech.QUEUE_FLUSH, map);
                textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String s) {

                        Log.d("Speak onStart", "1");
                    }

                    @Override
                    public void onDone(String s) {

                        Log.d("Speak onDone", "1");

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (Util.checkInternet(BootActivity.this)) {

                            textToSpeech.speak(Constants.SPEAK_WIFI_CONNECTED, TextToSpeech.QUEUE_FLUSH, map);

                            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                @Override
                                public void onStart(String s) {

                                }

                                @Override
                                public void onDone(String s) {

                                    Intent mainIntent = new Intent(BootActivity.this, MainActivity.class);
                                    startActivity(mainIntent);
                                }

                                @Override
                                public void onError(String s) {

                                }
                            });

                        }
                        else {

                            textToSpeech.speak(Constants.SPEAK_WIFI_NOT_CONNECTED, TextToSpeech.QUEUE_FLUSH, map);

                            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                @Override
                                public void onStart(String s) {

                                }

                                @Override
                                public void onDone(String s) {

                                    Intent mainIntent = new Intent(BootActivity.this, MainActivity.class);
                                    startActivity(mainIntent);
                                }

                                @Override
                                public void onError(String s) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onError(String s) {
                        Log.d("Speak onError ", "1");
                    }
                });

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.d("TTS", "Error in converting Text to Speech!");
                }
            }
        }, 200);
    }

//    private void initSpeaker() {
//
//        Log.d(TAG, "Step3");
//
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                int speechStatus = textToSpeech.speak(Constants.DEFWELCOMETEXT, TextToSpeech.QUEUE_FLUSH, null);
//                if (speechStatus == TextToSpeech.ERROR) {
//                    Log.d("TTS", "Error in converting Text to Speech!");
//                }
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                if (Util.checkInternet(BootActivity.this)) {
//
//                    textToSpeech.speak(Util.SPEAK_WIFI_CONNECTED, TextToSpeech.QUEUE_FLUSH, map);
//
//                    textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
//                        @Override
//                        public void onStart(String s) {
//
//                        }
//
//                        @Override
//                        public void onDone(String s) {
//
//                            Intent mainIntent = new Intent(BootActivity.this, MainActivity.class);
//                            startActivity(mainIntent);
//                        }
//
//                        @Override
//                        public void onError(String s) {
//
//                        }
//                    });
//
//                }
//                else {
//
//                    textToSpeech.speak(Util.SPEAK_WIFI_CONNECTED, TextToSpeech.QUEUE_FLUSH, map);
//
//                    textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
//                        @Override
//                        public void onStart(String s) {
//
//                        }
//
//                        @Override
//                        public void onDone(String s) {
//
//                            Intent mainIntent = new Intent(BootActivity.this, MainActivity.class);
//                            startActivity(mainIntent);
//                        }
//
//                        @Override
//                        public void onError(String s) {
//
//                        }
//                    });
//                }
//
//                Log.d(TAG, "Step4");
//
//                Intent mainIntent = new Intent(BootActivity.this, MainActivity.class);
//                startActivity(mainIntent);
//            }
//        }, 200);
//    }


//    private void initDevice(final String colorValue, final String text) {
//
//        final Runnable r = new Runnable() {
//            public void run() {
//
//                int DEFAULT_PIN = 911;
//                int red_pin= 89 + DEFAULT_PIN;          /*Define the Red Pin number*/
//                int green_pin = 68 + DEFAULT_PIN;       /*Define the Green Pin number*/
//                int blue_pin = 69 + DEFAULT_PIN;
//                /*Define the Blue Pin number*/
//                /*Check for Valid conditions for the RGB LED*/
//
//                char red_value = colorValue.charAt(0);
//                char green_value = colorValue.charAt(1);
//                char blue_value = colorValue.charAt(2);
//
//                mgpio.setGpioStatus(red_pin, Integer.parseInt(String.valueOf(red_value)));              /*Set the gpio status based on the value passed through arguement*/
//                mgpio.setGpioStatus(green_pin, Integer.parseInt(String.valueOf(green_value)));          /*Set the gpio status based on the value passed through arguement*/
//                mgpio.setGpioStatus(blue_pin, Integer.parseInt(String.valueOf(blue_value)));
//
//               // mHandler.postDelayed(this, 1000);
//
//                Log.d("Handler", "Running Handler");
//
//                Log.d("TTS", "button clicked: " + defaultText);
//                int speechStatus = textToSpeech.speak(defaultText, TextToSpeech.QUEUE_FLUSH, null);
//
//                if (speechStatus == TextToSpeech.ERROR) {
//                    Log.d("TTS", "Error in converting Text to Speech!");
//                }
//                //mHandler.postDelayed(this, 2000);
//                Intent mainIntent = new Intent(BootActivity.this, LCDActivity.class);
//                startActivity(mainIntent);
//
//
//            }
//        };
//    }

//    @Override
//    public void run() {       /*Run the thread*/
//
//        /*Wait for 2Sec after reading/writing for 2Sec, before closing the app*/
////        try {
////            gpio_thread.sleep(2000);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
////        /*Close the app*/
////        finishAffinity();
////        System.exit(0);
//        /*Close the app*/
//    }


    static {
        System.loadLibrary("native-lib");
    }


}