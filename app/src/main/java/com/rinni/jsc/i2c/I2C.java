package com.rinni.jsc.i2c;


/**
 * Serial Communication Management
 * the device name of first uart: /dev/ttymxc1
 * the device name of second uart: /dev/ttymxc2
 */  
public class I2C<I2CRead_1native> {
	
	/** uart1 device name */
	public static final String UART_DRIVER_NODE1="/dev/ttyHSL0";
	/** uart2 device name */
	public static final String UART_DRIVER_NODE2="/dev/ttyHSL1";

	/** NON_BLOCK */
	public static final int NON_BLOCK=0;
	/** BLOCK */
	public static final int BLOCK=1;
	
	/**
	 * the file description of opening I2C device
	 */
	private int fd=-1;
	
	/**
	 * block mode
	 */
	private int blockModel=NON_BLOCK;


	public int open(String port){
		fd=open_native(port);
		blockModel=NON_BLOCK;
		return fd>0?0:-1;
	}
	
	/**
	 * Close the communications port.
	 */
	public void close(int fd){
		close_native(fd);
	}
	


	public byte I2Cread(byte slave_addr, byte read_addr, int size)
	{
		return I2CRead_1native(slave_addr,read_addr, size, fd);
	}

	public int I2CWrite(byte slave_addr, byte[] buf, int size)
	{

//        Log.d("Buff value in Hexa 3", buf.toString());
		return I2CWrite_1native(slave_addr, buf, size,fd);
	}

	public void PutChar(int xval, int yval,String buf)
	{

//        Log.d("Buff value in Hexa 3", buf.toString());
		PutChar_1native(xval,yval,buf, fd);
	}

	public void LCDInit(){
		LCDInit_1native(fd);
	}

	public void IO_set(int gpio, boolean direction){

		setDirection_1native(gpio, direction, fd);
	}
	public void IO_out(int gpio, int value){

		setValue_1native(gpio, value, fd);
	}


	public boolean IO_input(int gpio){

		return getValue_1native(gpio, fd);
	}

	///////////////////////////I2C Part//////////////////////////////////////

	private native byte I2CRead_1native(byte slave_addr, byte addr, int size, int fd );
	//	private native byte I2CRead_1native(int slave_addr, int[] bufArr, int size, int fd);
	private native int open_native(String uartName);
	private native void close_native(int fd);
	private native byte I2CWrite_1native(byte slave_addr, byte[] buffer, int size, int fd );
	private native void PutChar_1native(int xval, int yval, String data, int fd);
	private native void LCDInit_1native(int fd);
	private native void setDirection_1native(int gpio, boolean direction, int fd);
	private native void setValue_1native(int gpio, int value, int fd);
	private native boolean getValue_1native(int gpio, int fd);


//	private native int Command_Write_1native(byte slave_addr, byte data, int fd );
//	private native int Data_Write_1native(byte slave_addr, byte data, int fd );
//	private native int I2CWrite_1native(int i2c_adr, int sub_adr,
//						int buf[], int Length, int fileHander);

	///////////////////////////I2C Part//////////////////////////////////////
}

