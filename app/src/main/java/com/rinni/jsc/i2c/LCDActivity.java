package com.rinni.jsc.i2c;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.rinni.jsc.R;


public class LCDActivity extends AppCompatActivity {
    //LCD

    private I2C mI2C;
    private static final int MESSAGE_LOG = 1;
    private Thread mThread;
    private boolean isflag = true;
    private boolean print_flag = false;
    private static boolean lcd_flag;
    private String mRb_str, mRb1_str;
    private byte[] hex_buffer ;
    private int fd;
    private String str="";
    private String io_exp_pin = "";
    private String io_dir = "";
    private String io_value = "";
    private String io_off = "";
    private boolean io_exp_off=false;
    String line1 = "";                                                                   /*Initialize the String to store the value for Line1 Lcd Message*/
    String line2 = "";                                                                   /*Initialize the String to store the value for Line2 Lcd Message*/
    int row1 = 0;
    int row2 = 1;

    String defaultText = "Welcome Rinira";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i2c);

        //LCD
        //Get Intent
        Intent intent = getIntent();

        if (intent.hasExtra("string"))                                                /*Check if the received parameter from Command line contains "string" as label, if contains store the string to the str string*/
        {
            str = intent.getStringExtra("string");
            lcd_flag = true;
            io_exp_off=false;
        }
        else if((intent.hasExtra("io_exp_pin")) && ((intent.hasExtra("io_exp_dir")) || (intent.hasExtra("io_exp_val"))))    /*Check if the received parameter from Command line contains "io_exp_dir" or "io_exp_pin" or "io_exp_val"as label, if contains store the string to the str string*/
        {
            io_exp_pin = intent.getStringExtra("io_exp_pin");                          /*Copy the pin number*/
            io_dir = intent.getStringExtra("io_exp_dir");                              /*Copy the pin direction*/
            if(io_dir.equals("o") || io_dir.equals("O"))                                     /*Check if pin direction is output*/
            {
                io_value = intent.getStringExtra("io_exp_val");                        /*Copy the value to the local string*/
            }
            io_exp_off = false;
            lcd_flag = false;
        }
        else if((intent.hasExtra("io_exp_off")))                                       /*Make IO expander pins for default, to reduce current consumption*/
        {
            io_off=intent.getStringExtra("io_exp_off");
            io_exp_off = true;
        }
        else {
            str = defaultText;                                                 /*If any extra other than label "string" or related to io expander is found, print the default text on LCD*/
            lcd_flag = true;
            io_exp_off = false;
        }

        mI2C = new I2C();// new

        /****************Auto Print on app open********************************/

        try {
            fd = mI2C.open("/dev/i2c-1");                                                /*Open the i2c port*/
        }
        catch(Exception e) {
            Toast.makeText(LCDActivity.this," /dev/i2c-1 open failure!",Toast.LENGTH_SHORT).show();
            return;
        }

        if(lcd_flag == true) {
            Log.d("IO_EXP","LCD SET");
            mI2C.IO_set(3,false);                                              /*Make LCD_RST high*/
            mI2C.IO_out(3, 1);

            mI2C.IO_set(2,false);                                              /*Enable GPS_EN pin of I/O expander*/
            mI2C.IO_out(2, 1);

            mI2C.LCDInit();                                                                   /*Initialise LCD*/
            int len = str.length();                                                           /*Get the length of the string received from command line*/

            if(len >= 32) {                                                                     /*Length greater than 32?*/
                len = 32;                                                                     /*Limit the text to be printed on LCD to 32*/
            }

            if(len > 16) {

                /*Extract the string to be printed on 1st line and 2nd line of the LCD*/
                for(int k=0; k<16; k++) {
                    line1 += str.charAt(k);                                                   /*Data to be printed on 1st line of the LCD */
                }
                for(int l=16;l<len;l++) {
                    line2 += str.charAt(l);                                                   /*Data to be printed on 2nd line of the LCD */
                }
                /*Display the data on 1st line of the LCD*/
                mI2C.PutChar(0,row1,line1);                                              /*Put Byte by Byte to display on LCD*/
                try {
                    mThread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /*Display the data on 2nd line of the LCD*/
                mI2C.PutChar(0,row2,line2);
                try {
                    mThread.sleep(500);


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                navigateRFID();

            }
            else {
                /*Display the data received from command line*/
                mI2C.PutChar(0,row1,str);
                try {
                    mThread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        else if((lcd_flag == false)&&(io_exp_off == false)) {                                  /*If io expander configuration has to done?*/

            Log.d("IO Expander","IO Set");
            boolean io_exp_dir=true;
            if(io_dir.equals("o")) {                                                            /*Direction Output?*/
                io_exp_dir = false;
            }
            else if (io_dir.equals("i")) {
                io_exp_dir = true;
            }
            else {
                Log.d("Expander","Invalid value");
            }

            Log.d("IO Expander","IO pin "+io_exp_pin+" IO State: "+io_value);
            mI2C.IO_set(Integer.parseInt(io_exp_pin),io_exp_dir);                                /*Set the direction of the pin*/
            if(io_exp_dir == false) {                                                              /*Direction Output?*/
                mI2C.IO_out(Integer.parseInt(io_exp_pin), Integer.parseInt(io_value));           /*Set the pin to the value sent from command line*/
            }
            else if(io_exp_dir == true) {                                                          /*Direction Input?*/
                boolean pin_value = mI2C.IO_input(Integer.parseInt(io_exp_pin));
                Log.d("IO Expander","IO pin "+io_exp_pin+" IO State: "+pin_value);    /*Get the state of the pin, and display on Logcat*/
            }
        }
        else if(io_exp_off == true) {                                                             /*IO Expander to be turned OFF?*/
            Log.d("IO Expander","IO OFF");
            /*Set the I/O expander pins in a state where the consumption will be less*/
            mI2C.IO_set(0,false);
            mI2C.IO_out(0,0);
            mI2C.IO_set(1,false);
            mI2C.IO_out(1,0);
            mI2C.IO_set(2,false);
            mI2C.IO_out(2,0);
            try {
                mThread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mI2C.IO_set(3,false);
            mI2C.IO_out(3,1);
            mI2C.IO_set(4,false);
            mI2C.IO_out(4,0);
            mI2C.IO_set(5,false);
            mI2C.IO_out(5,0);
            try {
                mThread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mI2C.IO_set(6,false);
            mI2C.IO_out(6,0);

//            mI2C.IO_out(0, Integer.parseInt(io_value));
        }

        try {
            mThread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        /*Auto Close the application after finishing of displaying*/
//        finishAffinity();
//        System.exit(0);
        /*Auto Close the application after finishing of displaying*/

        /*********************Auto Print on app open***************************/

    }


    private void navigateRFID() {
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent mainIntent = new Intent(LCDActivity.this, MainActivity.class);
//                startActivity(mainIntent);
//            }
//        }, 1000);
    }

}