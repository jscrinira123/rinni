﻿//#include "JNIHelp.h"
#include "jni.h"
#include <fcntl.h>
#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <cstdlib>
#include "ql_log.h"
#include <unistd.h>
#include <linux/i2c-dev.h>
//#include <linux/list.h>
#include <linux/i2c.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <iostream>
#include <string>

#define I2C_CLOCK                  100000
#define ROWS                       2
#define COLUMNS                    16
#define ST7032_ADDR               0x3E

#define PCA9534_ADDR              0x20




/* lcd screen states */


#define  READY                          1
#define  TRANSMIT                       2
#define  NOCONN                         3
#define  BUFFULL                        4
#define  BATLOW                         5
#define  BATFUL							6
//#define  DEFAULT                        7
//extern Config_Parameter FactorySettings;
//extern RTC_TIME_T FullTime;
uint8_t DDRAM_Data[ROWS][COLUMNS];
uint8_t DDRAM_Address = 0;
uint8_t Entry_Mode = 0x06;
uint8_t Increment = 1;
uint8_t Shift = 0;
uint8_t Display = 0x0C;
uint8_t Power_Icon_Contrast =0x5C;
uint8_t Contrast = 0x3F;
uint8_t IconRAM[16];
uint8_t Icon_Table[13][2] = {
        {0x00, 0x10},
        {0x02, 0x10},
        {0x04, 0x10},
        {0x06, 0x10},
        {0x07, 0x10},
        {0x07, 0x08},
        {0x09, 0x10},
        {0x0B, 0x10},
        {0x0D, 0x10},
        {0x0D, 0x10},
        {0x0D, 0x04},
        {0x0D, 0x02},
        {0x0F, 0x10}};
static uint8_t bit_pin1;


//int errno;
namespace android
{
    static void throw_NullPointerException(JNIEnv *env, const char* msg)
    {
        jclass clazz;
        clazz = env->FindClass("java/lang/NullPointerException");
        env->ThrowNew(clazz, msg);
    }

    int uartSetSerial(int speed,int databits,int stopbits, char parity,int fd){
        int i;
        int status;
        int speed_arr[] = {B115200, B38400, B19200, B9600, B4800, B2400, B1200, B300,
                           B38400, B19200, B9600, B4800, B2400, B1200, B300 };
        int name_arr[] = {115200,38400,  19200,  9600,  4800,  2400,  1200,  300,      38400, 19200,  9600, 4800, 2400, 1200,  300 };

        struct termios options;

        if(tcgetattr( fd,&options)!=0){
            perror("SetupSerial 1");
            return -1;
        }

        //设置串口输入波特率和输出波特率
        for ( i= 0; i<sizeof(speed_arr)/sizeof(int); i++){
            if  (speed == name_arr[i]){
                cfsetispeed(&options, speed_arr[i]);
                cfsetospeed(&options, speed_arr[i]);
            }
        }

        //修改控制模式，保证程序不会占用串口
        options.c_cflag |= CLOCAL;

        //修改控制模式，使得能够从串口中读取输入数据
        options.c_cflag |= CREAD;

        //设置数据位
        options.c_cflag &= ~CSIZE; //屏蔽其他标志位
        switch (databits){
            case 5    :
                options.c_cflag |= CS5;
                break;
            case 6    :
                options.c_cflag |= CS6;
                break;
            case 7    :
                options.c_cflag |= CS7;
                break;
            case 8:
                options.c_cflag |= CS8;
                break;
            default:
                fprintf(stderr,"Unsupported data size/n");
                return (-1);
        }

        //设置校验位
        switch (parity){
            case 'n':
            case 'N': //无奇偶校验位。
                options.c_cflag &= ~PARENB;
                options.c_iflag &= ~INPCK;
                break;
            case 'o':
            case 'O'://设置为奇校验
                options.c_cflag |= (PARODD | PARENB);
                options.c_iflag |= INPCK;
                break;
            case 'e':
            case 'E'://设置为偶校验
                options.c_cflag |= PARENB;
                options.c_cflag &= ~PARODD;
                options.c_iflag |= INPCK;
                break;
            case 's':
            case 'S': //设置为空格
                options.c_cflag &= ~PARENB;
                options.c_cflag &= ~CSTOPB;
                break;
            default:
                fprintf(stderr,"Unsupported parity/n");
                return (-1);
        }

        // 设置停止位
        switch (stopbits){
            case 1:
                options.c_cflag &= ~CSTOPB;
                break;
            case 2:
                options.c_cflag |= CSTOPB;
                break;
            default:
                fprintf(stderr,"Unsupported stop bits/n");
                return (-1);
        }

        //修改输出模式，原始数据输出
        options.c_lflag  &= ~(ICANON | ECHO | ECHOE | ISIG);  /*Input*/
        options.c_oflag &= ~OPOST;

        //设置等待时间和最小接收字符

        options.c_cc[VTIME] = 1; /* 读取一个字符等待1*(1/10)s */

        options.c_cc[VMIN] = 1; /* 读取字符的最少个数为1 */

        options.c_iflag &=~(ICRNL | IGNCR );

        //如果发生数据溢出，接收数据，但是不再读取

        tcflush(fd,TCIFLUSH);

        //激活配置 (将修改后的termios数据设置到串口中）

        if (tcsetattr(fd,TCSANOW,&options) != 0)

        {

            perror("com set error!/n");

            return (-1);

        }

        return (0);

    }
/**
  * @brief  Read Command for I2c devices
  * @param1  slave_addr : Address for the slave device
  * @param2  read_addr  : Register value to be read from slave devices
  * @param3  length     : Length of the read register value that to be passed
  * @param4  fd         : file descriptor
  * @retval  jbyte      : return the value of byte written
  */
    jbyte I2CRead(unsigned char slave_addr, unsigned char read_addr, int length, int fd)
    {

        unsigned char rddata[2];
        struct i2c_rdwr_ioctl_data ioctl_data;
        struct i2c_msg msgs[2];
        int res = 0;
        unsigned long funcs;
        res = ioctl(fd, I2C_SLAVE_FORCE, slave_addr);
        if (res < 0) {
            LOGE("I2C: Can't set slave address");
            LOGD("ERROR: ioctl failed and returned errno %s",strerror(errno));
            return -1;
        }
        LOGD("Slave Set Successfully in READ");
        for(int i=0; i<10000;i++);

        unsigned char write_buff[1];
        write_buff[0] = 0x00;
        write(fd,write_buff,1);
        int ret = read(fd,rddata,1);
        LOGD("Read Bytes:  %x",rddata[0]);

        if(ret < 0){
            LOGD("ERROR READING: %d ",ret);
            LOGD("ERROR Read: ioctl failed and returned errno %s",strerror(errno));
            if(errno == ETIMEDOUT){
            }
            if(errno == EINVAL){

            }
            return -1;
        }
        if(ret >= 0){
            LOGD("READ SUCCESS ");
            return rddata[0];
        }

    free(rddata);

    }
/**
  * @brief  Write Command for I2c devices
  * @param1  slave_addr : Address for the slave device
  * @param2  write_buff : Register value to be write to slave devices
  * @param3  size       : Length of the write register value that to be passed as param2
  * @param4  fd         : file descriptor
  * @retval  unsigned char : return the value of byte written
  */
    unsigned char I2CWrite(unsigned char slave_addr, unsigned char *write_buff, unsigned char size, int fd)
    {

        struct i2c_rdwr_ioctl_data ioctl_data;
        struct i2c_msg msgs[2];
        unsigned long funcs;

        int j =0,res = 0;

        LOGD("Buff value in Hexa 1= %x, %x",write_buff[0], write_buff[1]);

        res = ioctl(fd, I2C_SLAVE_FORCE, slave_addr);
        if (res != 0) {
            LOGE("I2C: Can't set slave address");
            return -1;
        }
        LOGD("Slave Set Successfully");
        for(int i=0; i<10000;i++);
        j = write(fd, write_buff, 2);
        usleep(20);
        if ( j != size) {
            LOGD("ERROR WRITING:  %d",j);
            /* ERROR HANDLING: i2c transaction failed */
            LOGD("ERROR Write: ioctl failed and returned errno %s",strerror(errno));
            return -1;
        }
        else{
            LOGD("Write Successful: %d",j);
            return j;
        }



    }

/**
  * @brief  Write Command to ST7032i
  * @param1  slave_addr : Command Data
  * @param2  Data       : Value to be write to the register
  * @param3  fd         : File descriptor
  * @retval None
  */
void I2C_Command_Write(unsigned char slave_addr, char Data,int fd)
{
    unsigned char buf[2];
    buf[0] = 0x80; //0x80;//B00000000;
    buf[1] = Data;
    I2CWrite(slave_addr, buf, 2, fd);
    return;
}
/**
  * @brief  Configure as Output
  * @param
  * @retval None
  */
void IO_expander_Config_out(uint8_t pin, bool direction, int fd)
{
//    static uint8_t bit_pin1;
    unsigned char config[2];


    if(direction == true)
        bit_pin1 |= (0x01 << (pin));                                //set bit to 1    //set as input
    else
        bit_pin1 &= ~(0x01 << (pin));                               //set bit to 0 for output
    usleep(27);
    config[0] = 0x03;                                               //for configuration
    config[1] = bit_pin1;
    usleep(27);
    I2CWrite(PCA9534_ADDR ,config ,2, fd);                          // set pin as input
    usleep(27);
}

/**
  * @brief  Set a value for a pin
  * @param
  * @retval None
  */
void IO_writePin(uint8_t pin, bool value, int fd)
{
    uint8_t bit = 0x00;
    bit = I2CRead(PCA9534_ADDR,0x01,1,fd);
    unsigned char val[2];

    if(value == true)												//True for input register configuration
        bit |= (1<<(pin));
    else                                                            //False for output register configuration
        bit &= ~(1<<(pin));

    val[0] = 0x01;                                                  //for write to output register
    val[1] = bit;
    I2CWrite(PCA9534_ADDR ,val ,2, fd);                             // set pin level
}

/**
  * @brief  Function to read from I/o Expander
  * @param1  pin        : Pin value of I/o Expander to pass
  * @param3  fd         : File descriptor
  * @retval bool        : Return value from register
  */
bool IO_readPin(unsigned char pin, int fd)
{

    uint8_t byte = 0;
    unsigned char read_addr[1];
    read_addr[0] = 0x00;
    byte = I2CRead(PCA9534_ADDR, read_addr[0],1,fd);                 //read the value from an input register
    LOGD("BYTE_VALUE: %x", byte);

    if(byte & (1 << pin))                                           //condition to check each bit of an input register
    {
        return true;                                                //return true if logic bit is high
    }

    return false;                                                   //return false if logic bit is low
}

/**
* @brief  Write Data to ST7032i
* @param  Data : "Data" Data
* @retval None
*/
void I2C_Data_Write(unsigned char slave_addr,unsigned char Data,int fd)
{
    unsigned char data[2];
    data[0]= 0xC0;
    data[1] = Data;
//    I2CWrite( data, 2);
//Write_I2C(ST7032_ADDR, data, 2);
    I2CWrite(slave_addr,data,2,fd);
    return;

}


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
* @brief  Clear Display
* @param  None
* @retval None
*/
void ST7032i_Clear(int fd)
{
    uint32_t i;

    I2C_Command_Write(ST7032_ADDR,0x01,fd);
    DDRAM_Address = 0;
    for(i=0;i<16;i++)
    {
        DDRAM_Data[0][i] =  ' ';
        DDRAM_Data[1][i] =  ' ';
    }
    usleep(1000);
}

/**
* @brief  put on display
* @param  None
* @retval None
*/
void ST7032i_Display_On(int fd)
{
    I2C_Command_Write(ST7032_ADDR,(( Display & 0xFB) | 0x04), fd);
    usleep(27);
}
/**
* @brief  display under line cursor
* @param  None
* @retval None
*/
void ST7032i_Cursor_On(int fd)
{

    I2C_Command_Write(ST7032_ADDR,(( Display & 0xFD) | 0x02),fd );
    usleep(27);
}

/**
* @brief  make square cursor brink
* @param  None
* @retval None
*/
void ST7032i_Cursor_Blink_On(int fd)
{

    I2C_Command_Write(ST7032_ADDR,(( Display & 0xFE) | 0x01),fd );
    usleep(27);
}

///**
/* @brief  Set display contrast. value is to be 0 - 63
* @param  contrast: contrast
* @retval None
*/
void ST7032i_Set_Contrast(uint8_t contrast,int fd)
{
    //Contrast set
    I2C_Command_Write(ST7032_ADDR,(0x70 | (contrast & 0x00)),fd);
    usleep(27);
    //Power/Icon/Contrast control
    I2C_Command_Write(ST7032_ADDR,(Power_Icon_Contrast | ( (contrast >> 4) & 0x03 )),fd);
    usleep(27);
}

///**
//* @brief  put character on st7032i lcd
//* @param  None
//* @retval None
//*/
void ST7032i_Putchar(uint8_t chardata,int fd)
{
    uint32_t i;
    I2C_Data_Write(ST7032_ADDR,(uint8_t)chardata,fd);
    usleep(27);
    if(DDRAM_Address < 0x10)
    {
        DDRAM_Data[0][DDRAM_Address] = chardata;
    }
    else if (DDRAM_Address >= 0x40 && DDRAM_Address < 0x50)
    {
        DDRAM_Data[1][DDRAM_Address - 0x40] = chardata;
    }
    if (Shift == 0)
    {
        DDRAM_Address = DDRAM_Address + Increment;
    }
    else if (Shift == 1 && Increment == 1)
    {
        for (i = 0 ; i< 15; i++)
        {
            DDRAM_Data[0][i] = DDRAM_Data[0][i+1];
            DDRAM_Data[1][i] = DDRAM_Data[1][i+1];
        }
        DDRAM_Data[0][15] = ' ';
        DDRAM_Data[1][15] = ' ';
    }
    else if (Shift == 1 && Increment == -1)
    {
        for (i = 15 ; i> 0; i--)
        {
            DDRAM_Data[0][i] = DDRAM_Data[0][i-1];
            DDRAM_Data[1][i] = DDRAM_Data[1][i-1];
        }
        DDRAM_Data[0][0] = ' ';
        DDRAM_Data[1][0] = ' ';
    }

    if (DDRAM_Address == 0x10)
    {
        DDRAM_Address = 0x40;
        I2C_Command_Write(ST7032_ADDR,(0x80 | DDRAM_Address),fd);
        usleep(27);
    }

    if (DDRAM_Address == 0x3F)
    {
        DDRAM_Address = 0x0F;
        I2C_Command_Write(ST7032_ADDR,(0x80 | DDRAM_Address),fd);
        usleep(27);
    }

    if (DDRAM_Address == 0xFF)
    {
        DDRAM_Address = 0x0;
        I2C_Command_Write(ST7032_ADDR,(0x80 | DDRAM_Address),fd);
        usleep(27);
    }

    if (DDRAM_Address == 0x50)
    {
        for(i=0;i<16;i++)
        {
            DDRAM_Data[0][i] =  DDRAM_Data[1][i];
            I2C_Command_Write(ST7032_ADDR,(0x80 |  (0x00 + i)),fd);
            I2C_Data_Write(ST7032_ADDR,(DDRAM_Data[0][i]),fd);
        }
        for(i=0;i<16;i++)
        {
            DDRAM_Data[1][i] =  ' ';
            I2C_Command_Write(ST7032_ADDR,(0x80 |  (0x40 + i)),fd);
            I2C_Data_Write(ST7032_ADDR,(DDRAM_Data[1][i]),fd);
        }
        DDRAM_Address = 0x40;
        I2C_Command_Write(ST7032_ADDR,(0x80 |  DDRAM_Address),fd);
        usleep(27);
    }
}

/*@brief:   Display the character at particular c and y position
 * @params: pos_x:  x position the character must be printed on LCD
 *          pos_y:  y position the character must be printed on LCD
 *          chardata:   charcter to be displayed at any position
 *  @return nothing*/
void ST7032i_Putchar_XY(int pos_x,int pos_y, char chardata,int fd)
{
	uint32_t i;

    if(pos_x == 0)
    {
        DDRAM_Address = pos_x;
    }
    else
    {
        DDRAM_Address=pos_x-1;
    }

    if(pos_y == 0)
    {
        DDRAM_Address=DDRAM_Address & 0x0F;
        DDRAM_Data[pos_y][DDRAM_Address] = chardata;
        I2C_Command_Write(ST7032_ADDR,(0x80 |  DDRAM_Address),fd);
        usleep(27);

    }
    if(pos_y==1)
    {
        DDRAM_Address=DDRAM_Address & 0x7F;
        DDRAM_Address=DDRAM_Address | 0x40;
        DDRAM_Data[pos_y][DDRAM_Address] = chardata;
        I2C_Command_Write(ST7032_ADDR,(0x80 |  DDRAM_Address),fd);
        usleep(27);
    }
    I2C_Data_Write(ST7032_ADDR,chardata,fd);
    usleep(27);

}
/*@description: Function to print the data on the LCD display at the particular position
 * @params: pos_x:  column position to be printed
 *          pos_y:  row position to be printed
 *          String[]:   String to be displayed on the LCD
 *          fd: File descriptor
 * @retutrn nothing*/
void ST7032i_Print_String_XY(int pos_x,int pos_y,const char String[],int fd)
{
    uint8_t i = 0;
    while(String[i] != '\0')
    {
        pos_x++;
        ST7032i_Putchar_XY(pos_x,pos_y,String[i],fd);

        if((pos_y==1)&&(pos_x >= 0x10))
        {
            pos_x = 0;
            pos_y=0;
        }
        if((pos_y==0)&&(pos_x >= 0x10))
        {
            pos_x = 0;
            pos_y=1;
        }
        i++;
    }
}

///**
//* @brief  Initialize ST7032i LCD and I2C interface
//* @param  None
//* @retval None
//*/
void ST7032i_Init(int fd)
{

    //Function Set
    I2C_Command_Write(ST7032_ADDR,(0x38),fd);
    usleep(27);
    //Function Set
    I2C_Command_Write(ST7032_ADDR,(0x39),fd);
    usleep(27);
    //Bias and OSC frequency
    I2C_Command_Write(ST7032_ADDR,(0x14),fd);
    usleep(27);
    //Contrast set
    I2C_Command_Write(ST7032_ADDR,(0x70),fd);
    usleep(27);

    //Power/Icon/Contrast control
    I2C_Command_Write(ST7032_ADDR,Power_Icon_Contrast,fd);
    usleep(27);

    //Contrast set
//        I2C_Command_Write(ST7032_ADDR,(Contrast),fd);
    ST7032i_Set_Contrast(0x3F,fd);
    //Follower control
    I2C_Command_Write(ST7032_ADDR,0x6C,fd);
    usleep(27);
    //Function Set
    I2C_Command_Write(ST7032_ADDR,0x39,fd);
    usleep(27);
    //Entry mode
    I2C_Command_Write(ST7032_ADDR,Entry_Mode,fd);
    usleep(27);
    I2C_Command_Write(ST7032_ADDR,Display,fd);
    usleep(27);
    //Clear
    ST7032i_Clear(fd);


}



/*@brief:   CLose the I2C port*/
static void I2CClose(int fd)
{
    close(fd);
}


//////////////////////////////////////////////////////////////////////////////////////////////////
    int uartWrite(char *send_buf,int data_len,int fd){
        int len = 0;
        LOGW("send_buf: %s",send_buf);
        len = write(fd,send_buf,data_len);
        if (len!=data_len ){
            LOGW("<<<>>>>has send data %d, but not equal %d",len,data_len);
        }else{
            LOGW("send data to uart: %d, fd is %d",len,fd);
        }
        return len;
    }





    static int portOpen(char const* deviceName){
        LOGE("uartOpen()-->:deviceName = %s",deviceName);
        int fd=open(deviceName,O_RDWR|O_NONBLOCK);//读写方式
        if(fd<0){
            LOGE("uartOpen()-->:fd open failure");
            return -1;
        }

        LOGW("uartOpen()-->: open device success");
        return fd;

    }

    int set_mode(int nMode,int showLog, int mTtyfd){

        LOGW("set_mode:nMode%d,nshowLog=%d",nMode,showLog);
        struct termios options;
        struct termios options_read;

        if(tcgetattr(mTtyfd,&options) != 0){
            LOGE("setup serial failure");
            return -1;
        }

        if(false && showLog == 1){
            LOGI("=============read termios=============");
            LOGI("options c_cflag.CS7:%d,CS8:%d",options.c_cflag & CS7,options.c_cflag & CS8);
            LOGI("options c_cflag.PARENB:%d,PARODD:%d",options.c_cflag & PARENB,options.c_cflag & PARODD);
            LOGI("options c_iflag.INPCK%d,ISTRIP:%d",options.c_iflag & INPCK,options.c_iflag & ISTRIP);
            LOGI("option c_ispeed:%d,c_ospeed:%d",cfgetispeed(&options) ,cfgetospeed(&options));
            LOGI("options c_cflag.CSTOPB:%d,",options.c_cflag & CSTOPB);
            LOGI("options c_cc.VTIME:%d,VMIN:%d",options.c_cc[VTIME],options.c_cc[VMIN]);
            LOGI("options c_cflag.CLOCAL:%d,CREAD:%d",options.c_cflag & CLOCAL,options.c_cflag&CREAD);
            LOGI("options c_lflag.ICANON:%d,ECHO:%d,ECHOE:%d,ISIG:%d",options.c_lflag & ICANON,options.c_lflag&ECHO,options.c_lflag&ECHOE,options.c_lflag&ISIG);
            LOGI("options c_oflag.OPOST:%d,",options.c_oflag &OPOST);
            LOGI("=============read termios endi=============");
        }


        if(nMode==0){
            options.c_iflag &=~(IXON | IXOFF);
            options.c_cflag &=~(CRTSCTS);
        }else if(nMode==1){
            options.c_iflag |=(IXON | IXOFF);
            options.c_cflag &=~(CRTSCTS);
        }else if(nMode==2){
            options.c_iflag &=~(IXON | IXOFF);
            options.c_cflag |=(CRTSCTS);
        }else if(nMode==3){
            options.c_iflag |=(IXON | IXOFF);
            options.c_cflag |=(CRTSCTS);
        }

        if(tcsetattr(mTtyfd,TCSANOW,&options) != 0){
            LOGE("tcsetattr device fail");
            return -1;
        }

        if(tcgetattr(mTtyfd,&options_read) != 0){
            LOGE("setup serial failure");
            return -1;
        }

        if(false && showLog == 1){
            LOGI("=============write termios=============");
            LOGI("options_read c_cflag.CS7:%d,CS8:%d",options_read.c_cflag & CS7,options_read.c_cflag & CS8);
            LOGI("options_read c_cflag.PARENB:%d,PARODD:%d",options_read.c_cflag & PARENB,options_read.c_cflag & PARODD);
            LOGI("options_read c_iflag.INPCK%d,ISTRIP:%d",options_read.c_iflag & INPCK,options_read.c_iflag & ISTRIP);
            LOGI("options_read c_ispeed:%d,c_ospeed:%d",cfgetispeed(&options_read) ,cfgetospeed(&options_read));
            LOGI("options_read c_cflag.CSTOPB:%d,",options_read.c_cflag & CSTOPB);
            LOGI("options_read c_cc.VTIME:%d,VMIN:%d",options_read.c_cc[VTIME],options_read.c_cc[VMIN]);
            LOGI("options c_cflag.CLOCAL:%d,CREAD:%d",options_read.c_cflag & CLOCAL,options_read.c_cflag&CREAD);
            LOGI("options_read c_lflag.ICANON:%d,ECHO:%d,ECHOE:%d,ISIG:%d",options_read.c_lflag & ICANON,options_read.c_lflag&ECHO,options_read.c_lflag&ECHOE,options_read.c_lflag&ISIG);
            LOGI("options_read c_oflag.OPOST:%d,",options_read.c_oflag &OPOST);
            LOGI("=============write termios end=============");
        }

        return 0;

    }

    static int uart_readable(int timeout,int fd)
    {
        int ret;
        fd_set set;
        struct timeval tv = { timeout / 1000, (timeout % 1000) * 1000 } ;

        FD_ZERO (&set);
        FD_SET (fd, &set);

        ret = select (fd + 1, &set, NULL, NULL, &tv);

        if (ret > 0){
            return 1;
        }

        return 0;
    }

    static int uart_read(char* buf, int size, int timeout,int fd)
    {
        LOGI("uart_read select model......");

        int got = 0, ret;
        do {
            ret = read (fd, buf + got, size - got);
            LOGI("ret %d.....",ret);
            LOGI("buff1 %s....",buf);
            if (ret > 0 ) got += ret;
            if (got >= size) break;
        }
        while (uart_readable(timeout,fd));

        LOGI("got %d.....",got);
        return got;
    }

    static int uartread(char* readBuff,int buffSize,int fd,int uartBlock){
        int readCount=0;
        if(uartBlock==O_NONBLOCK){
            readCount=uart_read(readBuff,buffSize,100,fd);
        }else{
            readCount=read(fd,readBuff,buffSize);
        }
        if(readCount<0){
            LOGI("read uart data under NONBLOCK error: %d",errno);
        }else{
            LOGI("read uart data: %d; fd is %d",readCount,fd);
        }
        return readCount;
    }

    static int uart_select(JNIEnv *env, jobject clazz,jint timeout,jint fd){
        int ret;
        fd_set set;
        struct timeval tv = { timeout / 1000, (timeout % 1000) * 1000 } ;

        FD_ZERO (&set);
        FD_SET (fd, &set);

        ret = select (fd + 1, &set, NULL, NULL, &tv);

        if (ret > 0){
            return 1;
        }

        return 0;
    }

    static int open2(JNIEnv *env, jobject clazz, jstring uartName){
        const char* uartNameStr = env->GetStringUTFChars(uartName,0);
        int openInt=portOpen(uartNameStr);
        return openInt;
    }

    static void close(JNIEnv *env, jobject clazz,jint fd){
        I2CClose(fd);
    }

    static int setBlock(JNIEnv *env, jobject clazz, jint blockmode,jint fd){
        int oldflags=fcntl(fd,F_GETFL,0);
        if(oldflags==-1){
            LOGE("serial set block error");
            return -1;
        }
        if(blockmode==0){
            oldflags |= O_NONBLOCK;
        }else{
            oldflags &= ~O_NONBLOCK;
        }
        int setFlags=fcntl(fd,F_SETFL,oldflags);
        LOGI("serial set block value=%d",setFlags);
        return 0;
    }

    static int setSerialPortParams(JNIEnv *env, jobject clazz, jint baudrate,jint dataBits,jint stopBits,jint parity,jint fd){
        return uartSetSerial(baudrate,dataBits,stopBits,parity,fd);
    }

    static int setFlowControlMode(JNIEnv *env, jobject clazz, jint flowcontrol,jint fd){
        LOGI("serial set flow control: %d",flowcontrol);
        return set_mode(flowcontrol,0,fd);
    }


    static int read2(JNIEnv *env, jobject clazz,jbyteArray buf,jint bufSize,jint fd,jint uartBlock){
        LOGI("read data from uart to buffer[%d]",bufSize);
        jbyte* arrayData=(jbyte*)env->GetByteArrayElements(buf,0);
        int readCount=uartread((char*)arrayData,bufSize,fd,uartBlock);
        env->ReleaseByteArrayElements(buf,arrayData,0);
        return readCount;
    }

    static int write1(JNIEnv *env, jobject clazz, jbyteArray buf, jint bufSize,jint fd){
        jbyte* arrayData = (jbyte*)env->GetByteArrayElements(buf,0);
        jsize arrayLength = env->GetArrayLength(buf);
        char* byteData = (char*)arrayData;
        int len = (int)arrayLength;
        int writeCount=uartWrite(byteData,bufSize,fd);
        env->ReleaseByteArrayElements(buf,arrayData,0);
        return writeCount;
    }

    static JNINativeMethod method_table[] = {
            { "close_native", "(I)V", (void*)close },
            { "open_native", "(Ljava/lang/String;)I", (void*)open2 },
            { "setBlock_native", "(II)I", (void*)setBlock },
            { "setSerialPortParams_native", "(IIIII)I", (void*)setSerialPortParams },
            { "setFlowControlMode_native", "(II)I", (void*)setFlowControlMode },
            { "select_native", "(II)I", (void*)uart_select},
            { "read_native", "([BIII)I", (void*)read2 },
            { "write_native", "([BII)I", (void*)write1 },
    };







    /******************I2C Read*************************/
    extern "C"
    JNIEXPORT jbyte  JNICALL
    Java_com_rinni_jsc_i2c_I2C_I2CRead_11native(JNIEnv *env, jobject thiz,
                                     jbyte slave_addr,  jbyte addr,
                                     jint size, jint fd) {
//        jbyte* arrayData = (jbyte*)env->GetByteArrayElements(data,0);
        return I2CRead(slave_addr, addr,size, fd);
    }

   /******************I2C Read*************************/

    /******************I2C Write*************************/
    extern "C"
    JNIEXPORT jbyte  JNICALL
    Java_com_rinni_jsc_i2c_I2C_I2CWrite_11native(JNIEnv *env, jobject thiz,jbyte slave_addr,
                                      jbyteArray buffer,jint size, jint fd){

        jbyte* arrayData = (jbyte*)env->GetByteArrayElements(buffer,0);

        unsigned char* byteData;
        byteData= (unsigned char*)arrayData;
        return I2CWrite(slave_addr, byteData, size, fd);
    }


    /******************I2C Write*************************/
    /******************LCD Init*************************/
    extern "C"
    JNIEXPORT void  JNICALL
    Java_com_rinni_jsc_i2c_I2C_LCDInit_11native(JNIEnv *env, jobject instance,jint fd){
        ST7032i_Init(fd);
        ST7032i_Display_On(fd);
        sleep(2);
        ST7032i_Clear(fd);

    }
    /******************LCD Init*************************/


    /******************GPIO Write*************************/
    extern "C"
    JNIEXPORT void  JNICALL
    Java_com_rinni_jsc_i2c_I2C_setDirection_11native(JNIEnv *env, jobject instance,jint gpio,
                                          jboolean direction, jint fd){


    usleep(1000);
        IO_expander_Config_out(gpio,direction,fd);          //Set pin as output
    usleep(1000);


    }


    extern "C"
    JNIEXPORT void  JNICALL
    Java_com_rinni_jsc_i2c_I2C_setValue_11native(JNIEnv *env, jobject instance,jint gpio, jint value, jint fd){



        IO_writePin(gpio,value,fd);

    }
/******************GPIO Write*************************/


/******************GPIO READ*************************/
    extern "C"
    JNIEXPORT bool  JNICALL
    Java_com_rinni_jsc_i2c_I2C_getValue_11native(JNIEnv *env, jobject instance,jint gpio, jint fd){


        return IO_readPin(gpio, fd);


    }
    /******************LCD Write*************************/
    extern "C"
    JNIEXPORT void  JNICALL
    Java_com_rinni_jsc_i2c_I2C_PutChar_11native(JNIEnv *env, jobject instance,jint x_val, jint y_val,
                                     jstring data, jint fd) {


        const char* mystring = env->GetStringUTFChars(data, 0);

        usleep(500);

        ST7032i_Print_String_XY(x_val,y_val,mystring,fd);               /*Display the data on the location passed through the arguements*/

    }
    /******************LCD Write*************************/
    /******************Port Open*************************/
    extern "C"
    jint
    Java_com_rinni_jsc_i2c_I2C_open_1native(JNIEnv *env, jobject instance, jstring uartName) {

        const char* s = env->GetStringUTFChars(uartName, 0);
        char item_value[128];
        strcpy(item_value, s);
        env->ReleaseStringUTFChars(uartName, s);
        LOGD("item_value = %s", item_value);
        return portOpen(item_value);
    }

    /******************Port Open*************************/

    /******************Port Close*************************/
    extern "C"
    void
    Java_com_rinni_jsc_i2c_I2C_close_1native(JNIEnv *env, jobject instance, jint fd) {
        I2CClose(fd);
    }

    /******************Port Close*************************/



    extern "C"
    jint
    Java_com_qucetel_andrew_serialjnidemo_I2C_read_1native(JNIEnv *env, jobject clazz,jbyteArray buf,jint bufSize,jint fd,jint uartBlock){
        LOGI("read data from uart to buffer[%d]",bufSize);
        jbyte* arrayData=(jbyte*)env->GetByteArrayElements(buf,0);
        int readCount=uartread((char*)arrayData,bufSize,fd,uartBlock);
        env->ReleaseByteArrayElements(buf,arrayData,0);
        return readCount;
    }



    extern "C"
    jstring
    Java_com_qucetel_andrew_serialjnidemo_I2C_read_1data_1native(JNIEnv *env, jobject instance,
                                                                 jbyteArray buf_, jint bufsize,
                                                                 jint timeout, jint fd) {
        char* data = (char*)env->GetByteArrayElements(buf_, NULL);
        int num = uart_read(data, bufsize, timeout, fd);
        jstring mStr = env->NewStringUTF(data);
        return mStr;
    }




    extern "C"
    jint
    Java_com_qucetel_andrew_serialjnidemo_I2C_setSerialPortParams_1native(JNIEnv *env,
                                                                          jobject instance,
                                                                          jint baudrate, jint dataBits,
                                                                          jint stopBits,
                                                                          jchar parity,
                                                                          jint fd) {

        jint mint = uartSetSerial(baudrate,dataBits,stopBits,parity,fd);
        LOGD("mint = %d", mint);
        return mint;
    }


};
