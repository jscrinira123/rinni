﻿
#include <stdio.h>
#include <jni.h>
#include "ql_log.h"
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>


#define DEVICE_NAME_TEST979 "/sys/class/gpio/gpio979/value"         //IO 68(Green)
#define DEVICE_NAME_TEST980 "/sys/class/gpio/gpio980/value"         //IO 69(Blue)
#define DEVICE_NAME_TEST1000 "/sys/class/gpio/gpio1000/value"       //IO 89(Red)
#define DEVICE_NAME_ADC	"/sys/class/power_supply/battery/mpp4_voltage"
#define ERROR  -1
namespace android
{

    static void throw_NullPointerException(JNIEnv *env, const char* msg)
    {
        jclass clazz;
        clazz = env->FindClass("java/lang/NullPointerException");
        env->ThrowNew(clazz, msg);
    }
    /*Set the GPIO pin to Low or High*/
    static int setGpioStatus(JNIEnv *env, jobject clazz, jint gpioId, jint gpioStatus){
        int fd=-1;
        if(gpioId==979){
            fd=open(DEVICE_NAME_TEST979,O_RDWR|O_NONBLOCK);
        }
        if(gpioId==980){
            fd=open(DEVICE_NAME_TEST980,O_RDWR|O_NONBLOCK);
        }
        if(gpioId==1000){
            fd=open(DEVICE_NAME_TEST1000,O_RDWR|O_NONBLOCK);
        }
        if(fd<0){
            LOGE("gpio%d open failure",gpioId);
            return ERROR;
        }
        int dataSize=1;
        char dataBuff[1];
        if(gpioStatus==0){
            dataBuff[0]='0';
        }else if(gpioStatus==1){
            dataBuff[0]='1';
        }else{
            return -1;
        }
        int writeCount=write(fd,dataBuff,dataSize);
        if(writeCount<=0){
            LOGE("gpio%d write failure",gpioId);
            return ERROR;
        }
        LOGE("set gpio%d status: %d, %d",gpioId,gpioStatus,dataBuff[0]);
        close(fd);
        return 0;
    }
    /*Gets the current status of the GPIO*/
    static int getGpioStatus(JNIEnv *env, jobject clazz, jint gpioId){
        char readBuff[1];
        int buffSize=1;
        int readCount=-1;

        int fd=-1;
        if(gpioId==979){
            fd=open(DEVICE_NAME_TEST979,O_RDWR|O_NONBLOCK);
        }
        if(gpioId==980){
            fd=open(DEVICE_NAME_TEST980,O_RDWR|O_NONBLOCK);
        }
        if(gpioId==1000){
            fd=open(DEVICE_NAME_TEST1000,O_RDWR|O_NONBLOCK);
        }
        if(fd<0){
            LOGE("gpio%d open failure",gpioId);
            return ERROR;
        }
        int readInfo=0;
        readCount=read(fd,readBuff,buffSize);
        if(readCount>0){
            readInfo=readBuff[0];
            if(readInfo>='0')readInfo=readInfo-'0';
        }
        else
        {
            LOGE("gpio%d read failure",gpioId);
            return ERROR;
        }
        close(fd);
        return readInfo;
    }
    static long int getAdcStatus(JNIEnv *env, jobject clazz)
    {
        char readBuff[8];
        int buffSize=8;
        int readCount=-1;
        int fd=-1;
        long int readInfo=0;
        fd=open(DEVICE_NAME_ADC,O_RDWR|O_NONBLOCK);
        if(fd<0)
        {
            LOGE("adc open failure\n");
            return ERROR;
        }
        readCount=read(fd,readBuff,buffSize);
        if(readCount>0)
        {
            readInfo=atoi(readBuff);
            LOGE("adc get readBuff=%s,readInfo=%ld\n",readBuff,readInfo);
        }
        else
        {
            LOGE("adc read failure\n");
            return ERROR;
        }
        close(fd);
        return readInfo;
    }

    static JNINativeMethod method_table[] = {
            { "getStatus_native", "(I)I", (void*)setGpioStatus },
            { "setStatus_native", "(II)V", (void*)getGpioStatus },
    };

/*int register_com_ipf_hardware_Do(JNIEnv *env){
    return AndroidRuntime::registerNativeMethods(
    		env,
    		"com/ipf/hardware/Do",
    		method_table, NELEM(method_table));
}*/
    extern "C"
    jint
    Java_com_rinni_jsc_GPIO_getGpioStatus
            (JNIEnv *env, jobject clazz, jint gpioId)
    {
//        LOGE("Jni getGpioStatus",gpioId);
        int ret = getGpioStatus(env,clazz,gpioId);
        return ret;
    }

    extern "C"
    jint
    Java_com_rinni_jsc_GPIO_setGpioStatus
            (JNIEnv *env, jobject clazz, jint gpioId, jint gpioStatus)
    {
        int ret = setGpioStatus(env,clazz,gpioId,gpioStatus);
        return ret;
    }

    extern "C"
    jint
    Java_com_rinni_jsc_GPIO_getAdcStatus
            (JNIEnv *env, jobject clazz, jint gpioId, jint gpioStatus)
    {
        int ret = getAdcStatus(env,clazz);
        return ret;
    }

};


